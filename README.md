[![pipeline status](https://gitlab.com/alphaDev/libTimeSeries/badges/master/pipeline.svg)](https://gitlab.com/alphaDev/libTimeSeries/commits/master)
[![coverage report](https://gitlab.com/alphaDev/libTimeSeries/badges/master/coverage.svg)](https://gitlab.com/alphaDev/libTimeSeries/commits/master)

# <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/4778612/libTimeSerie.png" alt="logo" width="40"/> libTimeSeries #

libTimeSeries is a java-language library that implement a time serie database engine.
It can store, long or String serie.

First benchmark result :

| variation                   |  Time             |
|-----------------------------|-------------------|
| TimeFixValueFix             | 4461401,810 ops/s |
| TimeFixValueVariation       | 4032570,053 ops/s |
| TimeVariationValueFix       | 4215026,455 ops/s |
| TimeVariationValueVariation | 3815682,051 ops/s |