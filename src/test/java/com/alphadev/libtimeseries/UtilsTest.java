package com.alphadev.libtimeseries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class UtilsTest {
	private static final Logger LOG = LogManager.getLogger(UtilsTest.class);

	private UtilsTest() {}

	public static String byteFormatLong(long value) {
		return StringUtils.leftPad(Long.toBinaryString(value), Long.SIZE, '0');
	}

	public static String byteFormatByte(byte value) {
		return StringUtils.leftPad(Integer.toBinaryString(value & 0xFF), Byte.SIZE, '0');
	}

	public static String byteFormatByteArray(byte[] value) {
		return Arrays.stream(ArrayUtils.toObject(value)).map(x -> UtilsTest.byteFormatByte(x)).reduce((x1, x2) -> x1 + x2).orElse("");
	}

	@SuppressWarnings("unchecked")
	public static <T> T getPrivateField(Object instance, String fieldName) {
		Field field;
		try {
			field = instance.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return (T) field.get(instance);
		}
		catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			LOG.error("Cannot get the field [{}] in type [{}]", fieldName, instance.getClass(), e);
			throw new TimeSeriesException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T getPrivateStaticField(Class<?> staticClass, String fieldName) {
		Field field;
		try {
			field = staticClass.getDeclaredField(fieldName);
			field.setAccessible(true);
			return (T) field.get(staticClass);
		}
		catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			LOG.error(e);
			throw new TimeSeriesException(e);
		}
	}

	public static void setPrivateField(Object instance, String fieldName, Object value) {
		Field field;
		try {
			field = instance.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(instance, value);
		}
		catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			LOG.error(e);
			throw new TimeSeriesException(e);
		}
	}

	public static void assertEqualsBitLong(String bufferName, long expect, long actual) {
		assertEquals(expect, actual, "bad buffer [" + bufferName + "] expect[" + byteFormatLong(expect) + "] actual[" + byteFormatLong(actual) + "]");
	}

	public static void assertEqualsBitLong(long expect, long actual) {
		assertEquals(expect, actual, "bad buffer expect[" + byteFormatLong(expect) + "] actual[" + byteFormatLong(actual) + "]");
	}

	public static void assertEqualsBitByte(byte expect, byte actual) {
		assertEquals(expect, actual, "bad buffer expect[" + byteFormatByte(expect) + "] actual[" + byteFormatByte(actual) + "]");
	}
}
