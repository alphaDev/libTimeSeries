package com.alphadev.libtimeseries.serie;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.LibTimeSeries;
import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.serie.entry.LongEntry;
import com.alphadev.libtimeseries.serie.entry.LongRange;
import com.alphadev.libtimeseries.serie.entry.StringEntry;
import com.alphadev.libtimeseries.synchronizer.SynchronizerType;

public class TestSerieRepository {
	private static final String DB_NAME = "testing";

	@BeforeEach
	public void setUp() {
		LibTimeSeries.load(DB_NAME, SynchronizerType.MEMORY);
	}

	@Test
	public void testAddNewSerie() {
		// Arrange
		ValueFormat valueFormat = ValueFormat.LONG;
		String unit = "ko";
		List<String> tags = Arrays.asList("testAddNewSerie-01", "testAddNewSerie-02", "testAddNewSerie-03");

		// Act
		SerieRepository.createOrUpdateSerie(new Metadata(tags, unit, valueFormat));

		// Assert
		Optional<Serie<LongEntry, Long>> actual = SerieRepository.findSerie(tags);
		assertTrue(actual.isPresent());
		assertEquals(tags, actual.get().getMetadata().getTags());
		assertEquals(unit, actual.get().getMetadata().getUnit());
		assertEquals(valueFormat, actual.get().getMetadata().getValueFormat());

		Optional<Serie<LongEntry, Long>> actual2 = SerieRepository.findSerie(Arrays.asList("testAddNewSerie-01", "testAddNewSerie-02"));
		assertFalse(actual2.isPresent());

		Optional<Serie<LongEntry, Long>> actual3 = SerieRepository.findSerie(Arrays.asList("testAddNewSerie-04", "testAddNewSerie-02"));
		assertFalse(actual3.isPresent());
	}

	@Test
	public void testDeleteSerie() {
		// Arrange
		ValueFormat valueFormat = ValueFormat.LONG;
		String unit = "ko";
		Set<List<String>> expectedBeforeDelete = new HashSet<>(Arrays.asList(Arrays.asList("testDeleteSerie-01"), Arrays.asList("testDeleteSerie-02"), Arrays.asList("testDeleteSerie-03")));
		Set<List<String>> expectedAfterDelete = new HashSet<>(Arrays.asList(Arrays.asList("testDeleteSerie-01"), Arrays.asList("testDeleteSerie-03")));

		SerieRepository.createOrUpdateSerie(new Metadata(Arrays.asList("testDeleteSerie-01"), unit, valueFormat));
		SerieRepository.createOrUpdateSerie(new Metadata(Arrays.asList("testDeleteSerie-02"), unit, valueFormat));
		SerieRepository.createOrUpdateSerie(new Metadata(Arrays.asList("testDeleteSerie-03"), unit, valueFormat));
		Set<List<String>> listExisingSerie = SerieRepository.listExisingSerie();
		assertEquals(expectedBeforeDelete, listExisingSerie);

		// Act
		SerieRepository.delete(Arrays.asList("testDeleteSerie-02"));
		SerieRepository.delete(Arrays.asList("testDeleteSerie"));

		// Assert
		listExisingSerie = SerieRepository.listExisingSerie();
		assertEquals(expectedAfterDelete, listExisingSerie);

	}

	@Test
	public void testUpdateSerie() {
		// Arrange
		ValueFormat valueFormat = ValueFormat.LONG;
		String unit = "ko";
		List<String> tags = Arrays.asList("testUpdateSerie-04");
		SerieRepository.addNewSerie(new Metadata(tags, unit, valueFormat));
		Optional<Serie<LongEntry, Long>> optionalSerie = SerieRepository.findSerie(tags);
		Serie<LongEntry, Long> serie = optionalSerie.get();
		serie.getMetadata().setUnit("mo");
		SerieRepository.createOrUpdateSerie(serie.getMetadata());

		// Act
		optionalSerie = SerieRepository.findSerie(tags);

		// Assert
		assertEquals(new Metadata(Arrays.asList("testUpdateSerie-04"), "mo", ValueFormat.LONG), optionalSerie.get().getMetadata());

	}

	@Test
	public void testAddValue() {
		// Arrange
		SerieRepository.createOrUpdateSerie(new Metadata(Arrays.asList("testAddValue-01"), "ko", ValueFormat.LONG));
		SerieRepository.createOrUpdateSerie(new Metadata(Arrays.asList("testAddValue-03"), "", ValueFormat.STRING));
		List<LongEntry> expectedLongValues = Arrays.asList(new LongEntry(01, 02), new LongEntry(02, 02), new LongEntry(03, 01), new LongEntry(04, 01), new LongEntry(05, 02), new LongEntry(06, 02));
		List<StringEntry> expectedStringValues = Arrays.asList(new StringEntry(01, "value01"), new StringEntry(02, "value02"), new StringEntry(03, "value01"));
		List<LongRange> expectedRangeValues = Arrays.asList(new LongRange(01, 03, 02l), new LongRange(03, 05, 01l), new LongRange(05, 06, 02l));

		// Act
		SerieRepository.add(Arrays.asList("testAddValue-01"), new LongEntry(01, 02));
		SerieRepository.add(Arrays.asList("testAddValue-01"), new LongEntry(02, 02));
		SerieRepository.add(Arrays.asList("testAddValue-01"), new LongEntry(03, 01));
		SerieRepository.add(Arrays.asList("testAddValue-01"), new LongEntry(04, 01));
		SerieRepository.add(Arrays.asList("testAddValue-01"), new LongEntry(05, 02));
		SerieRepository.add(Arrays.asList("testAddValue-01"), new LongEntry(06, 02));

		SerieRepository.add(Arrays.asList("testAddValue-03"), new StringEntry(01, "value01"));
		SerieRepository.add(Arrays.asList("testAddValue-03"), new StringEntry(02, "value02"));
		SerieRepository.add(Arrays.asList("testAddValue-03"), new StringEntry(03, "value01"));

		assertThrows(TimeSeriesException.class, () -> {
			SerieRepository.add(Arrays.asList("testAddValue-02"), new LongEntry(06, 02));
		});

		// Assert
		List<LongEntry> actualReadValue = SerieRepository.read(Arrays.asList("testAddValue-01"), 01, 06);
		assertEquals(expectedLongValues, actualReadValue);
		List<StringEntry> actualReadStringValue = SerieRepository.read(Arrays.asList("testAddValue-03"), 01, 06);
		assertEquals(expectedStringValues, actualReadStringValue);
		List<LongRange> actualRangeValue = SerieRepository.readRange(Arrays.asList("testAddValue-01"), 01, 06, 02);
		assertEquals(expectedRangeValues, actualRangeValue);

		assertThrows(TimeSeriesException.class, () -> {
			SerieRepository.read(Arrays.asList("testAddValue-02"), 01, 06);
		});
		assertThrows(TimeSeriesException.class, () -> {
			SerieRepository.readRange(Arrays.asList("testAddValue-02"), 01, 06, 02);
		});

	}

}
