package com.alphadev.libtimeseries.serie.driver;

import static com.alphadev.libtimeseries.UtilsTest.assertEqualsBitLong;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.UtilsTest;
import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferUtils;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.StringEntry;

public class TestEncodeStringDriver {
	private static final Logger	LOG					= LogManager.getLogger(TestEncodeStringDriver.class);
	private static final int	REF_DIFF_TIME_SIZE	= 32;

	@Test
	public void whenEncodeFirstEntry() throws UnsupportedEncodingException {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "testing";
		byte[] expectValueStringBytes = valueExpect.getBytes(StandardCharsets.UTF_8.name());
		long expectValueSize = expectValueStringBytes.length;

		StringEntry entry = new StringEntry(timestampExpect, valueExpect);
		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer();

		// Act
		StringEntryEncoder.getInstance().encodeInitialEntry(entry, buffer, cursor);

		// Assert
		cursor = new Cursor();
		long actualTimestamp = buffer.readLong(cursor, Long.SIZE);
		long actualStringSize = buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE_FOR_STRING);
		byte[] actualStringBytes = buffer.readBytes(cursor, (int) actualStringSize);

		assertEquals(timestampExpect, actualTimestamp);
		assertEquals(expectValueSize, actualStringSize);
		assertArrayEquals(expectValueStringBytes, actualStringBytes);
	}

	@Test
	public void whenEncodeWithSameTimeAndValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestampExpect, "t€stîng-%02");
		StringEntry entry = new StringEntry(timestampExpect + refDiffTime, "t€stîng-%02");
		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer(new long[1], Long.SIZE);

		// Act
		StringEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b11l << 62, buffer.getBufferArray()[0]);
	}

	@Test
	public void whenEncodeWithDiffTimeAndSameValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestampExpect, "t€stîng-%02");
		StringEntry entry = new StringEntry(timestampExpect + refDiffTime + 3, "t€stîng-%02");
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		StringEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b00000100111l << (53 - REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

	@Test
	public void whenEncodeWithDiffTimeNegAndSameValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestampExpect, "t€stîng-%02");
		StringEntry entry = new StringEntry(timestampExpect + refDiffTime - 3, "t€stîng-%02");
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		StringEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b0_000010111_1l << (53 - REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

	@Test
	public void whenEncodeWithSameTimeAndDiffValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestampExpect, "t€stîng-%02");
		StringEntry entry = new StringEntry(timestampExpect + refDiffTime, "t€stîng-%0a");
		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer(new long[1], Long.SIZE);

		byte[] lastEntryByte = lastEntry.getValueEncode();
		byte[] entryByte = entry.getValueEncode();

		String lastEntryByteString = UtilsTest.byteFormatByteArray(lastEntryByte);
		String entryByteString = UtilsTest.byteFormatByteArray(entryByte);
		LOG.debug("lastEntryByteString : " + lastEntryByteString);
		LOG.debug("entryByteString     : " + entryByteString);
		LOG.debug("7      : " + UtilsTest.byteFormatLong(7));

		// Act
		StringEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b1_0_0000_0001_0101_0011l << (Long.SIZE - 18), buffer.getBufferArray()[0]);
	}

	@Test
	public void whenEncodeWithDiffTimeAndDiffValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestampExpect, "t€stîng-%02");
		StringEntry entry = new StringEntry(timestampExpect + refDiffTime - 3, "t€stîng-%0a");
		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer(new long[1], Long.SIZE);

		// Act
		StringEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b0_000010111_0_0000_0001_0101_0011l << (Long.SIZE - 27), buffer.getBufferArray()[0]);
	}

}
