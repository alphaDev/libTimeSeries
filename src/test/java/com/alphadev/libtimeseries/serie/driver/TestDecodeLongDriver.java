package com.alphadev.libtimeseries.serie.driver;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferUtils;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.LongEntry;

public class TestDecodeLongDriver {
	private static final int REF_DIFF_TIME_SIZE = 32;

	@Test
	public void whenDecodeInitialEntry() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;

		long[] bufferInternal = new long[2];
		bufferInternal[0] = timestampExpect;
		bufferInternal[1] = valueExpect;
		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2);

		// Act
		LongEntry entry = LongEntryDecoder.getInstance().decodeInitialEntry(buffer, cursor, null);

		// Assert
		assertEquals(timestampExpect, entry.getTimestamp());
		assertEquals(valueExpect, (long) entry.getValue());
	}

	@Test
	public void whenDecoderefDiffTime() {
		// Arrange
		int refDiffTimeExpect = 10;

		long[] bufferInternal = new long[3];
		bufferInternal[0] = 0;
		bufferInternal[1] = 0;
		bufferInternal[2] = ((long) refDiffTimeExpect << REF_DIFF_TIME_SIZE);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2, Cursor.CURSOR_START_POS, 2);
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2 + Integer.SIZE);

		// Act
		int refDiffTime = LongEntryDecoder.getInstance().decodeRefDiffTime(buffer, cursor);

		// Assert
		assertEquals(refDiffTimeExpect, refDiffTime);
	}

	@Test
	public void whenDecodeWithSameTimeAndValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTimeExpect = 10;

		long[] bufferInternal = new long[3];
		bufferInternal[0] = timestampExpect;
		bufferInternal[1] = valueExpect;
		bufferInternal[2] = ((long) refDiffTimeExpect << REF_DIFF_TIME_SIZE) | (0b11l << 62 - REF_DIFF_TIME_SIZE);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2 + REF_DIFF_TIME_SIZE + 4);
		LongEntry prevEntry = new LongEntry(timestampExpect, valueExpect);

		// Act
		LongEntry entry = LongEntryDecoder.getInstance().decodeNextEntry(prevEntry, refDiffTimeExpect, buffer, cursor);

		// Assert
		assertEquals(timestampExpect + refDiffTimeExpect, entry.getTimestamp());
		assertEquals(valueExpect, (long) entry.getValue());
	}

	@Test
	public void whenDecodeWithDiffTimeAndSameValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTimeExpect = 10;

		long[] bufferInternal = new long[3];
		bufferInternal[0] = timestampExpect;
		bufferInternal[1] = valueExpect;
		bufferInternal[2] = ((long) refDiffTimeExpect << REF_DIFF_TIME_SIZE) | (0b00000100111l << 53 - REF_DIFF_TIME_SIZE);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2 + REF_DIFF_TIME_SIZE + 11);
		LongEntry prevEntry = new LongEntry(timestampExpect, valueExpect);

		// Act
		LongEntry entry = LongEntryDecoder.getInstance().decodeNextEntry(prevEntry, refDiffTimeExpect, buffer, cursor);

		// Assert
		assertEquals(timestampExpect + refDiffTimeExpect + 3, entry.getTimestamp());
		assertEquals(valueExpect, (long) entry.getValue());
	}

	@Test
	public void whenDecodeWithDiffTimeNegAndSameValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTimeExpect = 10;

		long[] bufferInternal = new long[3];
		bufferInternal[0] = timestampExpect;
		bufferInternal[1] = valueExpect;
		bufferInternal[2] = ((long) refDiffTimeExpect << REF_DIFF_TIME_SIZE) | (0b00000101111l << 53 - REF_DIFF_TIME_SIZE);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2 + REF_DIFF_TIME_SIZE + 11);
		LongEntry prevEntry = new LongEntry(timestampExpect, valueExpect);

		// Act
		LongEntry entry = LongEntryDecoder.getInstance().decodeNextEntry(prevEntry, refDiffTimeExpect, buffer, cursor);

		// Assert
		assertEquals(timestampExpect + refDiffTimeExpect - 3, entry.getTimestamp());
		assertEquals(valueExpect, (long) entry.getValue());
	}

	@Test
	public void whenDecodeWithSameTimeAndDiffValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTimeExpect = 10;

		long[] bufferInternal = new long[3];
		bufferInternal[0] = timestampExpect;
		bufferInternal[1] = valueExpect;
		bufferInternal[2] = ((long) refDiffTimeExpect << REF_DIFF_TIME_SIZE) | (0b100001001101l << 52 - REF_DIFF_TIME_SIZE);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2 + REF_DIFF_TIME_SIZE + 12);
		LongEntry prevEntry = new LongEntry(timestampExpect, valueExpect);

		// Act
		LongEntry entry = LongEntryDecoder.getInstance().decodeNextEntry(prevEntry, refDiffTimeExpect, buffer, cursor);

		// Assert
		assertEquals(timestampExpect + refDiffTimeExpect, entry.getTimestamp());
		assertEquals(valueExpect + 3, (long) entry.getValue());
	}

	@Test
	public void whenDecodeWithDiffTimeAndDiffValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTimeExpect = 10;

		long[] bufferInternal = new long[3];
		bufferInternal[0] = timestampExpect;
		bufferInternal[1] = valueExpect;
		bufferInternal[2] = ((long) refDiffTimeExpect << REF_DIFF_TIME_SIZE) | (0b000001011100001001101l << 43 - REF_DIFF_TIME_SIZE);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2 + REF_DIFF_TIME_SIZE + 21);
		LongEntry prevEntry = new LongEntry(timestampExpect, valueExpect);

		// Act
		LongEntry entry = LongEntryDecoder.getInstance().decodeNextEntry(prevEntry, refDiffTimeExpect, buffer, cursor);

		// Assert
		assertEquals(timestampExpect + refDiffTimeExpect - 3, entry.getTimestamp());
		assertEquals(valueExpect + 3, (long) entry.getValue());
	}

}
