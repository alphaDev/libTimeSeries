package com.alphadev.libtimeseries.serie.driver;

import static com.alphadev.libtimeseries.UtilsTest.assertEqualsBitLong;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferUtils;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.LongEntry;

public class TestEncodeLongDriver {
	private static final int REF_DIFF_TIME_SIZE = 32;

	@Test
	public void whenEncodeFirstEntry() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry = new LongEntry(timestampExpect, valueExpect);
		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer();

		// Act
		LongEntryEncoder.getInstance().encodeInitialEntry(entry, buffer, cursor);

		// Assert
		assertEquals(timestampExpect, buffer.getBufferArray()[0]);
		assertEquals(valueExpect, buffer.getBufferArray()[1]);
	}

	@Test
	public void whenEncodeRefDiffTime() {
		// Arrange
		int refDiffTime = 10;
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2, Cursor.CURSOR_START_POS, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		LongEntryEncoder.getInstance().encodeRefDiffTime(refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0, buffer.getBufferArray()[0]);
		assertEqualsBitLong(0, buffer.getBufferArray()[1]);
		assertEqualsBitLong(((long) refDiffTime << REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

	@Test
	public void whenEncodeWithSameTimeAndValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTime = 10;
		LongEntry lastEntry = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry = new LongEntry(timestampExpect + refDiffTime, valueExpect);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		LongEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b11l << (62 - REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

	@Test
	public void whenEncodeWithDiffTimeAndSameValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTime = 10;
		LongEntry lastEntry = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry = new LongEntry(timestampExpect + refDiffTime + 3, valueExpect);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		LongEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b00000100111l << (53 - REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

	@Test
	public void whenEncodeWithDiffTimeNegAndSameValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTime = 10;
		LongEntry lastEntry = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry = new LongEntry(timestampExpect + refDiffTime - 3, valueExpect);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		LongEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b00000101111l << (53 - REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

	@Test
	public void whenEncodeWithSameTimeAndDiffValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTime = 10;
		LongEntry lastEntry = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry = new LongEntry(timestampExpect + refDiffTime, valueExpect + 3);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		LongEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b100001001101l << (52 - REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

	@Test
	public void whenEncodeWithDiffTimeAndDiffValue() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		int refDiffTime = 10;
		LongEntry lastEntry = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry = new LongEntry(timestampExpect + refDiffTime - 3, valueExpect + 3);
		Cursor cursor = BufferUtils.getCursor(Long.SIZE * 2 + REF_DIFF_TIME_SIZE, Cursor.CURSOR_START_POS - REF_DIFF_TIME_SIZE, 2);
		Buffer buffer = BufferUtils.getTestBuffer(new long[3], Long.SIZE);

		// Act
		LongEntryEncoder.getInstance().encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);

		// Assert
		assertEqualsBitLong(0b000001011100001001101l << (43 - REF_DIFF_TIME_SIZE), buffer.getBufferArray()[2]);
	}

}
