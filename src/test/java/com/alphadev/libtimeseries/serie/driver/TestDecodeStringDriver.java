package com.alphadev.libtimeseries.serie.driver;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferUtils;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.StringEntry;

public class TestDecodeStringDriver {

	@Test
	public void whenDecodeFirstEntry() throws UnsupportedEncodingException {
		// Arrange
		long timestamp = 1513339350l;
		String value = "testing";
		StringEntry expectedEntry = new StringEntry(timestamp, value);
		byte[] valueBytes = value.getBytes(StandardCharsets.UTF_8.name());

		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, timestamp, Long.SIZE);
		buffer.appendLong(cursor, valueBytes.length, DriverConst.BIT_SIZE_OF_SIZE_FOR_STRING);
		buffer.appendBytes(cursor, valueBytes);

		// Act
		cursor = new Cursor();
		StringEntry actualEntry = StringEntryDecoder.getInstance().decodeInitialEntry(buffer, cursor, null);

		// Assert
		assertEquals(expectedEntry, actualEntry);
	}

	@Test
	public void whenDecodeWithSameTimeAndValue() {
		// Arrange
		long timestamp = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestamp, "t€stîng-%02");
		StringEntry expectedEntry = new StringEntry(timestamp + refDiffTime, "t€stîng-%02");

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b11, 2);

		// Act
		cursor = new Cursor();
		StringEntry actualEntry = StringEntryDecoder.getInstance().decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
	}

	@Test
	public void whenDecodeWithDiffTimeAndSameValue() {
		// Arrange
		long timestamp = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestamp, "t€stîng-%02");
		StringEntry expectedEntry = new StringEntry(timestamp + refDiffTime + 3, "t€stîng-%02");

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b0_000010_0_11_1l, 11);

		// Act
		cursor = new Cursor();
		StringEntry actualEntry = StringEntryDecoder.getInstance().decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
	}

	@Test
	public void whenDecodeWithDiffTimeNegAndSameValue() {
		// Arrange
		long timestamp = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestamp, "t€stîng-%02");
		StringEntry expectedEntry = new StringEntry(timestamp + refDiffTime - 3, "t€stîng-%02");

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b0_000010_1_11_1l, 11);

		// Act
		cursor = new Cursor();
		StringEntry actualEntry = StringEntryDecoder.getInstance().decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
	}

	@Test
	public void whenDecodeWithSameTimeAndDiffValue() {
		// Arrange
		long timestamp = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestamp, "t€stîng-%02");
		StringEntry expectedEntry = new StringEntry(timestamp + refDiffTime, "t€stîng-%0a");

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b1_0_0000_0001_0101_0011l, 18);

		// Act
		cursor = new Cursor();
		StringEntry actualEntry = StringEntryDecoder.getInstance().decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
	}

	@Test
	public void whenDecodeWithDiffTimeAndDiffValue() {
		// Arrange
		long timestamp = 1513339350l;
		int refDiffTime = 10;
		StringEntry lastEntry = new StringEntry(timestamp, "t€stîng-%02");
		StringEntry expectedEntry = new StringEntry(timestamp + refDiffTime - 3, "t€stîng-%0a");

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b0_000010_1_11_0_0000_0001_0101_0011l, 27);

		// Act
		cursor = new Cursor();
		StringEntry actualEntry = StringEntryDecoder.getInstance().decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);

	}

}
