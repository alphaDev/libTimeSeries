package com.alphadev.libtimeseries.serie.driver;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.UtilsTest;
import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferUtils;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.LongRange;

public class TestDecodeLongRangeDriver {
	private static final Logger		LOG					= LogManager.getLogger(TestDecodeLongRangeDriver.class);

	private final LongRangeDecoder	longRangeDecoder	= new LongRangeDecoder(10);

	@Test
	public void whenDecodeInitialEntryNoNewRange() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;

		LongRange prevEntry = new LongRange(0, timestamp - 5, value);
		LongRange expectEntry = new LongRange(0, timestamp, value);

		long[] bufferInternal = new long[] { timestamp, value };
		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2);

		// Act
		LongRange actualEntry = longRangeDecoder.decodeInitialEntry(buffer, cursor, prevEntry);

		// Assert
		assertEquals(expectEntry, actualEntry);
		assertEquals(expectEntry, prevEntry);
	}

	@Test
	public void whenDecodeInitialEntryNewRangeBecauseNoPrev() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;

		LongRange expectEntry = new LongRange(timestamp, timestamp, value);

		long[] bufferInternal = new long[] { timestamp, value };
		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2);

		// Act
		LongRange actualEntry = longRangeDecoder.decodeInitialEntry(buffer, cursor, null);

		// Assert
		assertEquals(expectEntry, actualEntry);
	}

	@Test
	public void whenDecodeInitialEntryNewRangeBecausePrevTooOld() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;

		LongRange prevEntry = new LongRange(0, timestamp - 15, value);
		LongRange expectEntry = new LongRange(timestamp, timestamp, value);

		long[] bufferInternal = new long[] { timestamp, value };
		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2);

		// Act
		LongRange actualEntry = longRangeDecoder.decodeInitialEntry(buffer, cursor, prevEntry);

		// Assert
		assertEquals(expectEntry, actualEntry);
		assertEquals(new LongRange(0, timestamp - 15, value), prevEntry);
	}

	@Test
	public void whenDecodeInitialEntryNewRangeBecausePrevNotSameValue() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;

		LongRange prevEntry = new LongRange(0, timestamp - 5, 0l);
		LongRange expectEntry = new LongRange(timestamp, timestamp, value);

		long[] bufferInternal = new long[] { timestamp, value };
		Cursor cursor = new Cursor();
		Buffer buffer = BufferUtils.getTestBuffer(bufferInternal, Long.SIZE * 2);

		// Act
		LongRange actualEntry = longRangeDecoder.decodeInitialEntry(buffer, cursor, prevEntry);

		// Assert
		assertEquals(expectEntry, actualEntry);
		assertEquals(new LongRange(0, timestamp, 0l), prevEntry);
	}

	@Test
	public void whenDecodeWithSameTimeAndValue() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;
		int refDiffTime = 10;

		LongRange lastEntry = new LongRange(0, timestamp, value);
		LongRange expectedEntry = new LongRange(0, timestamp + refDiffTime, value);

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b11, 2);

		// Act
		cursor = new Cursor();
		LongRange actualEntry = longRangeDecoder.decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
		assertEquals(new LongRange(0, timestamp + refDiffTime, value), lastEntry);
	}

	@Test
	public void whenDecodeWithDiffTimeAndSameValue() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;
		int refDiffTime = 5;

		LongRange lastEntry = new LongRange(0, timestamp, value);
		LongRange expectedEntry = new LongRange(0, timestamp + refDiffTime + 3, value);

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b0_000010_0_11_1l, 11);

		// Act
		cursor = new Cursor();
		LongRange actualEntry = longRangeDecoder.decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
		assertEquals(new LongRange(0, timestamp + refDiffTime + 3, value), lastEntry);
	}

	@Test
	public void whenDecodeWithDiffTimeAndSameValueAndMakeNewRangeBecausePrevTooOld() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;
		int refDiffTime = 10;

		LongRange lastEntry = new LongRange(0, timestamp, value);
		LongRange expectedEntry = new LongRange(timestamp + refDiffTime + 15, timestamp + refDiffTime + 15, value);

		LOG.debug("15 : " + UtilsTest.byteFormatLong(15l));
		LOG.debug(" 4 : " + UtilsTest.byteFormatLong(4l));

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b0_000100_0_1111_1l, 13);

		// Act
		cursor = new Cursor();
		LongRange actualEntry = longRangeDecoder.decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
		assertEquals(new LongRange(0, timestamp, value), lastEntry);
	}

	@Test
	public void whenDecodeWithDiffTimeNegAndSameValue() {
		// Arrange
		long timestamp = 1513339350l;
		long value = 5;
		int refDiffTime = 10;

		LongRange lastEntry = new LongRange(0, timestamp, value);
		LongRange expectedEntry = new LongRange(0, timestamp + refDiffTime - 3, value);

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b0_000010_1_11_1l, 11);

		// Act
		cursor = new Cursor();
		LongRange actualEntry = longRangeDecoder.decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
	}

	@Test
	public void whenDecodeWithSameTimeAndDiffValue() {
		// Arrange
		long timestamp = 1513339350l;
		int refDiffTime = 10;
		LongRange lastEntry = new LongRange(0, timestamp, 0l);
		LongRange expectedEntry = new LongRange(timestamp + refDiffTime, timestamp + refDiffTime, 5l);

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b1_0_000011_101l, 11);

		LOG.debug("0 : " + UtilsTest.byteFormatLong(0l));
		LOG.debug("5 : " + UtilsTest.byteFormatLong(5l));
		LOG.debug("3 : " + UtilsTest.byteFormatLong(3));

		// Act
		cursor = new Cursor();
		LongRange actualEntry = longRangeDecoder.decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
		assertEquals(new LongRange(0, timestamp + refDiffTime, 0l), lastEntry);
	}

	@Test
	public void whenDecodeWithDiffTimeAndDiffValue() {
		// Arrange
		long timestamp = 1513339350l;
		int refDiffTime = 10;
		LongRange lastEntry = new LongRange(0, timestamp, 0l);
		LongRange expectedEntry = new LongRange(timestamp + refDiffTime - 3, timestamp + refDiffTime - 3, 5l);

		Cursor cursor = BufferUtils.getTestCursor();
		Buffer buffer = BufferUtils.getTestBuffer();
		buffer.appendLong(cursor, 0b0_000010_1_11_0_000011_101l, 20);

		// Act
		cursor = new Cursor();
		LongRange actualEntry = longRangeDecoder.decodeNextEntry(lastEntry, refDiffTime, buffer, cursor);

		// Assert
		assertEquals(expectedEntry, actualEntry);
		assertEquals(new LongRange(0, timestamp + refDiffTime - 3, 0l), lastEntry);

	}

}
