package com.alphadev.libtimeseries.serie.entry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.serie.entry.LongEntry;

public class TestLongEntry {
	private static final Logger LOG = LogManager.getLogger(TestLongEntry.class);

	@Test
	public void whenSetEntryWithDirectValue() {
		// Arrange
		LongEntry entry = new LongEntry();
		long timestampExpect = 151333935l;
		long valueExpect = 5;

		// Act
		entry.setEntry(timestampExpect, valueExpect);

		// Assert
		assertEquals(timestampExpect, entry.getTimestamp());
		assertEquals(valueExpect, (long) entry.getValue());
	}

	@Test
	public void whenSetEntryWithOtherEntry() {
		// Arrange
		LongEntry entry = new LongEntry();
		long timestampExpect = 151333935l;
		long valueExpect = 5;
		LongEntry entryTmp = new LongEntry(timestampExpect, valueExpect);

		// Act
		entry.setEntry(entryTmp);

		// Assert
		assertEquals(timestampExpect, entry.getTimestamp());
		assertEquals(valueExpect, (long) entry.getValue());
	}

	@Test
	public void whenToString() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry = new LongEntry(timestampExpect, valueExpect);
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());

		// Act
		String value = entry.toString();

		// Assert
		LOG.debug(value);
		assertEquals("[" + formatter.format(Instant.ofEpochSecond(timestampExpect)) + " : 5]", value);
	}

	@Test
	public void whenEntryEqualsWithNull() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry = new LongEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry.equals(null);

		// Assert
		assertFalse(actual);
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void whenEntryEqualsWithString() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry = new LongEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry.equals("toto");

		// Assert
		assertFalse(actual);
	}

	@Test
	public void whenEntryEqualsWithSameInstance() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry = new LongEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry.equals(entry);

		// Assert
		assertTrue(actual);
	}

	@Test
	public void whenEntryNotEqualsWithDiffId() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry01 = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry02 = new LongEntry(timestampExpect, 10);

		// Act
		boolean actual = entry01.equals(entry02);

		// Assert
		assertFalse(actual);
	}

	@Test
	public void whenEntryEquals() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry01 = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry02 = new LongEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry01.equals(entry02);

		// Assert
		assertTrue(actual);
	}

	@Test
	public void whenHash() {
		// Arrange
		long timestampExpect = 1513339350l;
		long valueExpect = 5;
		LongEntry entry01 = new LongEntry(timestampExpect, valueExpect);
		LongEntry entry02 = new LongEntry(timestampExpect, valueExpect);

		// Act
		int actual01 = entry01.hashCode();
		int actual02 = entry02.hashCode();

		// Assert
		assertEquals(actual01, actual02);
	}

}
