package com.alphadev.libtimeseries.serie.entry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

public class TestStringEntry {
	private static final Logger LOG = LogManager.getLogger(TestStringEntry.class);

	@Test
	public void whenSetEntryWithDirectValue() {
		// Arrange
		StringEntry entry = new StringEntry();
		long timestampExpect = 151333935l;
		String valueExpect = "T€stîng@";

		// Act
		entry.setEntry(timestampExpect, valueExpect);

		// Assert
		assertEquals(timestampExpect, entry.getTimestamp());
		assertEquals(valueExpect, entry.getValue());
	}

	@Test
	public void whenSetEntryWithOtherEntry() {
		// Arrange
		StringEntry entry = new StringEntry();
		long timestampExpect = 151333935l;
		String valueExpect = "T€stîng@";
		StringEntry entryTmp = new StringEntry(timestampExpect, valueExpect);

		// Act
		entry.setEntry(entryTmp);

		// Assert
		assertEquals(timestampExpect, entry.getTimestamp());
		assertEquals(valueExpect, entry.getValue());
	}

	@Test
	public void whenGetAndSetValueEncode() {
		// Arrange
		long timestampExpect = 151333935l;
		String valueExpect = "T€stîng@";
		StringEntry entryExpect = new StringEntry(timestampExpect, valueExpect);

		// Act
		byte[] valueEncode01 = entryExpect.getValueEncode();
		byte[] valueEncode02 = entryExpect.getValueEncode();
		StringEntry entryActual = new StringEntry(timestampExpect, valueEncode01);

		// Assert
		assertEquals(entryActual, entryExpect);
		// Yes we intentionally check it is the same instance
		assertEquals(valueEncode01, valueEncode02);
	}

	@Test
	public void whenToString() {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "T€stîng@";
		StringEntry entry = new StringEntry(timestampExpect, valueExpect);
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());

		// Act
		String value = entry.toString();

		// Assert
		LOG.debug(value);
		assertEquals("[" + formatter.format(Instant.ofEpochSecond(timestampExpect)) + " : T€stîng@]", value);
	}

	@Test
	public void whenEntryEqualsWithNull() {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "T€stîng@";
		StringEntry entry = new StringEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry.equals(null);

		// Assert
		assertFalse(actual);
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void whenEntryEqualsWithString() {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "T€stîng@";
		StringEntry entry = new StringEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry.equals("toto");

		// Assert
		assertFalse(actual);
	}

	@Test
	public void whenEntryEqualsWithSameInstance() {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "T€stîng@";
		StringEntry entry = new StringEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry.equals(entry);

		// Assert
		assertTrue(actual);
	}

	@Test
	public void whenEntryNotEqualsWithDiffId() {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "T€stîng@";
		StringEntry entry01 = new StringEntry(timestampExpect, valueExpect);
		StringEntry entry02 = new StringEntry(timestampExpect, "zz");

		// Act
		boolean actual = entry01.equals(entry02);

		// Assert
		assertFalse(actual);
	}

	@Test
	public void whenEntryEquals() {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "T€stîng@";
		StringEntry entry01 = new StringEntry(timestampExpect, valueExpect);
		StringEntry entry02 = new StringEntry(timestampExpect, valueExpect);

		// Act
		boolean actual = entry01.equals(entry02);

		// Assert
		assertTrue(actual);
	}

	@Test
	public void whenHash() {
		// Arrange
		long timestampExpect = 1513339350l;
		String valueExpect = "T€stîng@";
		StringEntry entry01 = new StringEntry(timestampExpect, valueExpect);
		StringEntry entry02 = new StringEntry(timestampExpect, valueExpect);

		// Act
		int actual01 = entry01.hashCode();
		int actual02 = entry02.hashCode();

		// Assert
		assertEquals(actual01, actual02);
	}

}
