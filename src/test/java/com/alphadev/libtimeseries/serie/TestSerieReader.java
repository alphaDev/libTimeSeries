package com.alphadev.libtimeseries.serie;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.LibTimeSeries;
import com.alphadev.libtimeseries.serie.driver.LongEntryDecoder;
import com.alphadev.libtimeseries.serie.driver.LongEntryEncoder;
import com.alphadev.libtimeseries.serie.driver.LongRangeDecoder;
import com.alphadev.libtimeseries.serie.entry.LongEntry;
import com.alphadev.libtimeseries.serie.entry.LongRange;
import com.alphadev.libtimeseries.synchronizer.SynchronizerType;

public class TestSerieReader {
	private static final Logger	LOG		= LogManager.getLogger(TestSerieReader.class);

	private static final String	DB_NAME	= "testing";

	@BeforeEach
	public void setUp() {
		LibTimeSeries.load(DB_NAME, SynchronizerType.MEMORY);
	}

	@AfterEach
	public void tearDown() {
		LibTimeSeries.close();
	}

	@Test
	public void testFindBuffers() {
		// Arrange
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());
		UUID serieId = UUID.fromString("123-123-123-123-123");
		long timestamp = 1519649946l;
		long value = 12l;
		long refDiffTime = 10l;

		SerieWriter.setMaxDuration(refDiffTime * 10);
		List<LongEntry> expectEntries = Stream.iterate(0, i -> i + 1).limit(38).map(i -> new LongEntry(timestamp + refDiffTime * i, value)).collect(Collectors.toList());
		SerieWriter<LongEntry, Long> serieWriter = new SerieWriter<> (serieId, LongEntryEncoder.getInstance());
		expectEntries.forEach(serieWriter::add);

		// Act
		long from = timestamp + refDiffTime * 5;
		long to = timestamp + refDiffTime * 32;
		LOG.debug("find from [{}] to [{}]", formatter.format(Instant.ofEpochSecond(from)), formatter.format(Instant.ofEpochSecond(to)));
		SerieReader<LongEntry, Long> serieReader = new SerieReader<> (serieId, LongEntryDecoder.getInstance());
		List<LongEntry> actual = serieReader.read(from, to);

		// Assert
		assertEquals(expectEntries.subList(5, 33).stream().collect(Collectors.toList()), actual);
	}

	@Test
	public void testReadRange() {
		// Arrange
		UUID serieId = UUID.fromString("123-123-123-123-123");
		long timestamp = 0;
		long refDiffTime = 10l;

		SerieWriter.setMaxDuration(refDiffTime * 10);
		List<LongEntry> entries = Arrays.asList( //
		        new LongEntry(timestamp + refDiffTime * 0, 0), //
		        new LongEntry(timestamp + refDiffTime * 1, 0), //
		        new LongEntry(timestamp + refDiffTime * 2, 0), //
		        new LongEntry(timestamp + refDiffTime * 3, 1), //
		        new LongEntry(timestamp + refDiffTime * 4, 1), //
		        new LongEntry(timestamp + refDiffTime * 5, 1), //
		        new LongEntry(timestamp + refDiffTime * 6, 2), //
		        new LongEntry(timestamp + refDiffTime * 7, 1), //
		        new LongEntry(timestamp + refDiffTime * 8, 0), //
		        new LongEntry(timestamp + refDiffTime * 9, 0), //
		        new LongEntry(timestamp + refDiffTime * 10, 5), //
		        new LongEntry(timestamp + refDiffTime * 11, 5), //
		        new LongEntry(timestamp + refDiffTime * 12, 0) //
		);
		SerieWriter<LongEntry, Long> serieWriter = new SerieWriter<> (serieId, LongEntryEncoder.getInstance());
		entries.forEach(serieWriter::add);

		List<LongRange> expected = Arrays.asList( //
		        new LongRange(timestamp + refDiffTime * 0, timestamp + refDiffTime * 3, 0l), //
		        new LongRange(timestamp + refDiffTime * 3, timestamp + refDiffTime * 6, 1l), //
		        new LongRange(timestamp + refDiffTime * 6, timestamp + refDiffTime * 7, 2l), //
		        new LongRange(timestamp + refDiffTime * 7, timestamp + refDiffTime * 8, 1l), //
		        new LongRange(timestamp + refDiffTime * 8, timestamp + refDiffTime * 10, 0l), //
		        new LongRange(timestamp + refDiffTime * 10, timestamp + refDiffTime * 12, 5l), //
		        new LongRange(timestamp + refDiffTime * 12, timestamp + refDiffTime * 12, 0l)//
		);

		// Act
//		SerieReader serieReader = new SerieReader(serieId, ValueFormat.LONG, 10);
		SerieReader<LongRange, Long> reader = new SerieReader<>(serieId, new LongRangeDecoder(10));
		List<LongRange> actual = reader.read(timestamp, (long) (timestamp + refDiffTime * 12.5));

		// Assert
		assertEquals(expected, actual);
	}
}
