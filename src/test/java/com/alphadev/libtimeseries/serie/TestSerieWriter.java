package com.alphadev.libtimeseries.serie;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import com.alphadev.libtimeseries.LibTimeSeries;
import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.buffer.BufferRepository;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.driver.LongEntryEncoder;
import com.alphadev.libtimeseries.serie.entry.LongEntry;
import com.alphadev.libtimeseries.synchronizer.SynchronizerType;

public class TestSerieWriter {
	private static final Logger	LOG			= LogManager.getLogger(TestSerieWriter.class);
	private static final UUID	SERIE_ID	= UUID.fromString("123-123-123-123-123");
	private static final String	NO_BUFFER	= "No buffer found";

	@BeforeEach
	public void setUp() {
		LibTimeSeries.load("test", SynchronizerType.MEMORY);
		SerieWriter.setMaxDuration(SerieWriter.DEFAULT_MAX_DURATION);
		SerieWriter.setMaxSize(SerieWriter.DEFAULT_MAX_SIZE);
	}

	@AfterEach
	public void tearDown() {
		LibTimeSeries.close();
	}

	@Test
	public void whenAddFirstEntry() {
		// Arrange
		long timestamp = 1519649946l;
		long value = 12l;
		SerieWriter<LongEntry, Long> serieWriter = new SerieWriter<>(SERIE_ID, LongEntryEncoder.getInstance());
		LongEntry entry = new LongEntry(timestamp, value);

		// Act
		serieWriter.add(entry);

		// Assert
		Optional<Buffer> optional = BufferRepository.getLoadBuffer(new BufferId(SERIE_ID, timestamp));
		Buffer buffer = null;
		if (!optional.isPresent()) {
			fail(NO_BUFFER);
		}
		else {
			buffer = optional.get();
			assertEquals(Long.SIZE * 2l, buffer.getBitSize());
			assertEquals(timestamp, buffer.getBufferArray()[0]);
			assertEquals(value, buffer.getBufferArray()[1]);
		}

	}

	@Test
	public void whenAddNextEntry() {
		// Arrange
		long timestamp = 1519649946l;
		long value = 12l;
		long refDiffTime = 10l;
		SerieWriter<LongEntry, Long> serieWriter = new SerieWriter<>(SERIE_ID, LongEntryEncoder.getInstance());
		LongEntry entry01 = new LongEntry(timestamp, value);
		LongEntry entry02 = new LongEntry(timestamp + refDiffTime, value);

		// Act
		serieWriter.add(entry01);
		serieWriter.add(entry02);

		// Assert
		Optional<Buffer> optional = BufferRepository.getLoadBuffer(new BufferId(SERIE_ID, timestamp));
		Buffer buffer = null;
		if (!optional.isPresent()) {
			fail(NO_BUFFER);
		}
		else {
			buffer = optional.get();
			Cursor cursor = new Cursor();
			assertEquals(Long.SIZE * 2 + Integer.SIZE + 2l, buffer.getBitSize());
			assertEquals(timestamp, buffer.readLong(cursor, Long.SIZE));
			assertEquals(value, buffer.readLong(cursor, Long.SIZE));
			assertEquals(refDiffTime, buffer.readLong(cursor, Integer.SIZE));
			assertEquals(0b11l, buffer.readLong(cursor, 2));
		}
	}

	@Test
	public void whenAddNextEntryWithRefDiffTimePreset() {
		// Arrange
		long timestamp = 1519649946l;
		long value = 12l;
		long refDiffTime = 10l;
		SerieWriter<LongEntry, Long> serieWriter = new SerieWriter<>(SERIE_ID, (int) refDiffTime, LongEntryEncoder.getInstance());
		LongEntry entry01 = new LongEntry(timestamp, value);
		LongEntry entry02 = new LongEntry(timestamp + refDiffTime, value);

		// Act
		serieWriter.add(entry01);
		serieWriter.add(entry02);

		// Assert
		Optional<Buffer> optional = BufferRepository.getLoadBuffer(new BufferId(SERIE_ID, timestamp));
		Buffer buffer = null;
		if (!optional.isPresent()) {
			fail(NO_BUFFER);
		}
		else {
			buffer = optional.get();
			Cursor cursor = new Cursor();
			assertEquals(Long.SIZE * 2 + Integer.SIZE + 2l, buffer.getBitSize());
			assertEquals(timestamp, buffer.readLong(cursor, Long.SIZE));
			assertEquals(value, buffer.readLong(cursor, Long.SIZE));
			assertEquals(refDiffTime, buffer.readLong(cursor, Integer.SIZE));
			assertEquals(0b11l, buffer.readLong(cursor, 2));
		}
	}

	@Test
	public void whenAddNextNextEntryWithRefDiffTimePreset() {
		// Arrange
		long timestamp = 1519649946l;
		long value = 12l;
		long refDiffTime = 10l;
		SerieWriter<LongEntry, Long> serieWriter = new SerieWriter<>(SERIE_ID, (int) refDiffTime, LongEntryEncoder.getInstance());
		LongEntry entry01 = new LongEntry(timestamp, value);
		LongEntry entry02 = new LongEntry(timestamp + refDiffTime, value);
		LongEntry entry03 = new LongEntry(timestamp + refDiffTime + refDiffTime, value);

		// Act
		serieWriter.add(entry01);
		serieWriter.add(entry02);
		serieWriter.add(entry03);

		// Assert
		Optional<Buffer> optional = BufferRepository.getLoadBuffer(new BufferId(SERIE_ID, timestamp));
		Buffer buffer = null;
		if (!optional.isPresent()) {
			fail(NO_BUFFER);
		}
		else {
			buffer = optional.get();
			Cursor cursor = new Cursor();
			assertEquals(Long.SIZE * 2 + Integer.SIZE + 4l, buffer.getBitSize());
			assertEquals(timestamp, buffer.readLong(cursor, Long.SIZE));
			assertEquals(value, buffer.readLong(cursor, Long.SIZE));
			assertEquals(refDiffTime, buffer.readLong(cursor, Integer.SIZE));
			assertEquals(0b1111l, buffer.readLong(cursor, 4));
		}
	}

	@Test
	public void whenAddNextEntryAndMustSaveBuffer() throws InterruptedException {
		LOG.debug("whenAddNextEntryAndMustSaveBuffer");
		// Arrange
		long timestamp = 1519649946l;
		long value = 12l;
		long refDiffTime = 10l;
		SerieWriter.setMaxDuration(14l);
		SerieWriter<LongEntry, Long> serieWriter = spy(new SerieWriter<>(SERIE_ID, (int) refDiffTime, LongEntryEncoder.getInstance()));
		ArgumentCaptor<ToWriteBuffer> argument = ArgumentCaptor.forClass(ToWriteBuffer.class);

		LongEntry entry01 = new LongEntry(timestamp, value);
		LongEntry entry02 = new LongEntry(timestamp + refDiffTime, value);
		LongEntry entry03 = new LongEntry(timestamp + refDiffTime * 2, value);
		LongEntry entry04 = new LongEntry(timestamp + refDiffTime * 3, value);
		LongEntry entry05 = new LongEntry(timestamp + refDiffTime * 10, value);

		// Act
		serieWriter.add(entry01);
		serieWriter.add(entry02);
		serieWriter.add(entry03);
		serieWriter.add(entry04);
		serieWriter.add(entry05);
		serieWriter.add(entry05);

		// Assert
		Cursor cursor01 = new Cursor();
		Optional<Buffer> optional = BufferRepository.getLoadBuffer(new BufferId(SERIE_ID, timestamp));
		Buffer buffer01 = null;
		if (!optional.isPresent()) {
			fail(NO_BUFFER);
		}
		else {
			buffer01 = optional.get();
			assertEquals(Long.SIZE * 2 + Integer.SIZE + 4l, buffer01.getBitSize());
			assertEquals(timestamp, buffer01.readLong(cursor01, Long.SIZE));
			assertEquals(value, buffer01.readLong(cursor01, Long.SIZE));
			assertEquals(refDiffTime, buffer01.readLong(cursor01, Integer.SIZE));
			assertEquals(0b1111l, buffer01.readLong(cursor01, 4));
		}

		ToWriteBuffer toWriteBufferExpected = new ToWriteBuffer(buffer01, cursor01);

		optional = BufferRepository.getLoadBuffer(new BufferId(SERIE_ID, timestamp + refDiffTime * 3));
		Buffer buffer02 = null;
		if (!optional.isPresent()) {
			fail(NO_BUFFER);
		}
		else {
			buffer02 = optional.get();
			Cursor cursor02 = new Cursor();
			assertEquals(Long.SIZE * 2 + Integer.SIZE + 15l, buffer02.getBitSize());
			assertEquals(timestamp + refDiffTime * 3, buffer02.readLong(cursor02, Long.SIZE));
			assertEquals(value, buffer02.readLong(cursor02, Long.SIZE));
		}

		
		Thread.sleep(100);
		
		verify(serieWriter, times(2)).callWriteBuffer(argument.capture());
		ToWriteBuffer toWriteBuffer = argument.getAllValues().get(0);
		assertEquals(toWriteBufferExpected.getBuffeId(), toWriteBuffer.getBuffeId());
		assertEquals(toWriteBufferExpected.getBuffer(), toWriteBuffer.getBuffer());
		assertEquals(toWriteBufferExpected.getSizeBits(), toWriteBuffer.getSizeBits());
	}

	@Test
	public void whenAddNextEntryAndMustSaveBufferOnMaxSizeAndFail() {
		// Arrange
		long timestamp = 1519649946l;
		long value = 12l;
		long refDiffTime = 10l;
		SerieWriter.setMaxSize(Long.SIZE * 2 + Integer.SIZE + 2l);
		SerieWriter<LongEntry, Long> serieWriter = spy(new SerieWriter<>(SERIE_ID, (int) refDiffTime, LongEntryEncoder.getInstance()));
		doThrow(new TimeSeriesException("testing")).when(serieWriter).callWriteBuffer(any());

		LongEntry entry01 = new LongEntry(timestamp, value);
		LongEntry entry02 = new LongEntry(timestamp + refDiffTime, value);
		LongEntry entry03 = new LongEntry(timestamp + refDiffTime * 2, value);
		LongEntry entry04 = new LongEntry(timestamp + refDiffTime * 3, value);
		LongEntry entry05 = new LongEntry(timestamp + refDiffTime * 4, value);

		// Act
		serieWriter.add(entry01);
		serieWriter.add(entry02);
		serieWriter.add(entry03);

		assertThrows(TimeSeriesException.class, () -> {
			serieWriter.add(entry04);
			serieWriter.add(entry05);
		});

		// Assert
	}
}
