package com.alphadev.libtimeseries.buffer;

import static com.alphadev.libtimeseries.UtilsTest.assertEqualsBitLong;
import static com.alphadev.libtimeseries.UtilsTest.assertEqualsBitByte;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.BufferOverflowException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.UtilsTest;

public class TestBuffer {
	private static final Logger		LOG						= LogManager.getLogger(TestBuffer.class);

	private static final int		INITIAL_SIZE			= 2048;

	private static final String		EXPECT_BUFFER02			= "expectBuffer02";
	private static final String		EXPECT_BUFFER01			= "expectBuffer01";
	private static final String		PRINT_DOUBLE_BUFFER		= "expectBuffer01:[{}] expectBuffer02:[{}]";
	private static final String		EXPECT_BUFFER_FORMAT	= "expectBuffer:[{}]";

	private static final BufferId	TEST_BUFFER_ID			= new BufferId(UUID.fromString("123-123-123-123-123"), 1);

	@Test
	public void whenReadBoolean() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l << 63;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		boolean actual = buffer.readBit(cursor);

		// Assert
		assertTrue(actual);
		assertEquals(Long.SIZE - 2l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
		assertEquals(1, cursor.getBitCursor());
	}

	@Test
	public void whenReadBooleanOnBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[2];
		bufferInternal[0] = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE * 2);
		Cursor cursor = new Cursor(Long.SIZE, 0, 0);
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		boolean actual = buffer.readBit(cursor);

		// Assert
		assertTrue(actual);
		assertEquals(Long.SIZE - 1l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 1l, cursor.getBitCursor());
	}

	@Test()
	public void whenReadBooleanOnOverBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		try {
			for (int i = 0; i < Long.SIZE; i++) {
				buffer.readBit(cursor);
			}
		}
		catch (BufferOverflowException e) {
			fail();
		}

		// Act
		assertThrows(BufferOverflowException.class, () -> {
			buffer.readBit(cursor);
		});

		// Assert
	}

	@Test()
	public void whenReadBooleanOnOverBufferLimitAfterLongRead() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		try {
			buffer.readLong(cursor, Long.SIZE - 1);
			buffer.readBit(cursor);
		}
		catch (BufferOverflowException e) {
			fail();
		}

		// Act
		assertThrows(BufferOverflowException.class, () -> {
			buffer.readBit(cursor);
		});

		// Assert
	}

	@Test
	public void whenReadBooleanOnNextLong() {
		// Arrange
		long[] bufferInternal = new long[2];
		bufferInternal[0] = 1l;
		bufferInternal[1] = 1l << 63;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE * 2);
		Cursor cursor = new Cursor(Long.SIZE, 0, 0);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(bufferInternal[0]), UtilsTest.byteFormatLong(bufferInternal[1]));

		// Act
		boolean actual01 = buffer.readBit(cursor);
		boolean actual02 = buffer.readBit(cursor);
		boolean actual03 = buffer.readBit(cursor);

		// Assert
		assertTrue(actual01);
		assertTrue(actual02);
		assertFalse(actual03);
		assertEquals(Long.SIZE - 3l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 3l, cursor.getBitCursor());
	}

	@Test
	public void whenReadLong() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 0b1010101l << 57;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		long actual = buffer.readLong(cursor, 7);

		// Assert
		assertEqualsBitLong(0b1010101l, actual);
		assertEquals(Long.SIZE - 8l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
	}

	@Test
	public void whenReadLongAndReadNexByte() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 0b10101011l << 56;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		long actualLong = buffer.readLong(cursor, 7);
		boolean actualBoolean = buffer.readBit(cursor);

		// Assert
		assertEqualsBitLong(0b1010101l, actualLong);
		assertTrue(actualBoolean);
		assertEquals(Long.SIZE - 9l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
	}

	@Test
	public void whenReadLongOnBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[2];
		bufferInternal[0] = 0b1010101l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE * 2);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		long actual = buffer.readLong(cursor, 64);

		// Assert
		assertEqualsBitLong(0b1010101l, actual);
		assertEquals(Long.SIZE - 1l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
	}

	@Test()
	public void whenReadLongOnOverBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		try {
			for (int i = 0; i < 30; i++) {
				buffer.readBit(cursor);
			}
		}
		catch (BufferOverflowException e) {
			fail();
		}

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.readLong(cursor, Long.SIZE);
		});

		// Assert
	}

	@Test()
	public void whenReadLongOnOverBufferLimitLimit() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, 37);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		try {
			for (int i = 0; i < 30; i++) {
				buffer.readBit(cursor);
			}
		}
		catch (BufferOverflowException e) {
			fail();
		}

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.readLong(cursor, 8);
		});

		// Assert
	}

	@Test
	public void whenReadLongBeforeOverBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l << 27;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, 37);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		try {
			for (int i = 0; i < 30; i++) {
				buffer.readBit(cursor);
			}
		}
		catch (BufferOverflowException e) {
			fail();
		}

		// Act
		long actual = buffer.readLong(cursor, 7);

		// Assert
		assertEquals(1l, actual);
	}

	@Test
	public void whenReadLongOnTwoLongBuffer() {
		// Arrange
		long[] bufferInternal = new long[2];
		bufferInternal[0] = 0b111l;
		bufferInternal[1] = 0b101l << 61;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE * 2);
		Cursor cursor = new Cursor(Long.SIZE - 2, 2, 0);

		LOG.debug("bufferInternal[0]:[{}] bufferInternal[1]:[{}]", UtilsTest.byteFormatLong(bufferInternal[0]), UtilsTest.byteFormatLong(bufferInternal[1]));

		// Act
		long actual = buffer.readLong(cursor, 6);

		// Assert
		assertEqualsBitLong(0b111101l, actual);
		assertEquals(Long.SIZE - 4l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 4l, cursor.getBitCursor());
	}

	@Test()
	public void whenReadLongWithNegativeSize() {
		// Arrange
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor();

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.readLong(cursor, -1);
		});

		// Assert
	}

	@Test()
	public void whenReadLongWithOverSize() {
		// Arrange
		Buffer buffer = new Buffer(TEST_BUFFER_ID, new long[2], Long.SIZE * 2);
		Cursor cursor = new Cursor();

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.readLong(cursor, Long.SIZE + 1);
		});

		// Assert
	}

	@Test
	public void whenReadByte() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 0b1111_0011_0011l << 52;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		byte actual = buffer.readByte(cursor);

		// Assert
		assertEqualsBitByte((byte) 0b1111_0011, actual);
		assertEquals(Long.SIZE - 9l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
	}

	@Test
	public void whenReadBytes() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 0b1111_0011_0011_1011l << 48;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		byte[] actual = buffer.readBytes(cursor, 2);

		// Assert
		assertEquals(2, actual.length);
		assertEqualsBitByte((byte) 0b1111_0011, actual[0]);
		assertEqualsBitByte((byte) 0b0011_1011, actual[1]);
		assertEquals(47, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
	}

	@Test
	public void whenReadByteAndReadNexByte() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 0b1111_0011_1011l << 52;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		byte actualByte = buffer.readByte(cursor);
		boolean actualBoolean = buffer.readBit(cursor);

		// Assert
		assertEqualsBitByte((byte) 0b1111_0011, actualByte);
		assertTrue(actualBoolean);
		assertEquals(Long.SIZE - 10l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
	}

	@Test
	public void whenReadByteOnBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[2];
		bufferInternal[0] = 0b1111_0011l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE * 2);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		// Act
		buffer.readLong(cursor, 56);
		byte actual = buffer.readByte(cursor);

		// Assert
		assertEqualsBitByte((byte) 0b1111_0011l, actual);
		assertEquals(Long.SIZE - 1l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
	}

	@Test()
	public void whenReadByteOnOverBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		buffer.readLong(cursor, 57);

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.readByte(cursor);
		});

		// Assert
	}

	@Test()
	public void whenReadByteOnOverBufferLimitLimit() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, 37);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		buffer.readLong(cursor, 30);

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.readByte(cursor);
		});

		// Assert
	}

	@Test
	public void whenReadByteBeforeOverBufferLimit() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 1l << 27;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, 37);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(bufferInternal[0]));

		buffer.readLong(cursor, 29);

		// Act
		byte actual = buffer.readByte(cursor);

		// Assert
		assertEquals(1, actual);
	}

	@Test
	public void whenReadByteOnTwoLongBuffer() {
		// Arrange
		long[] bufferInternal = new long[2];
		bufferInternal[0] = 0b1010l;
		bufferInternal[1] = 0b1001l << 60;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE * 2);
		Cursor cursor = new Cursor(Long.SIZE - 3, 3, 0);

		LOG.debug("bufferInternal[0]:[{}] bufferInternal[1]:[{}]", UtilsTest.byteFormatLong(bufferInternal[0]), UtilsTest.byteFormatLong(bufferInternal[1]));

		// Act
		byte actual = buffer.readByte(cursor);

		// Assert
		assertEqualsBitByte((byte) 0b1010_1001, actual);
		assertEquals(Long.SIZE - 5l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 5l, cursor.getBitCursor());
	}

	@Test
	public void whenToString() {
		// Arrange
		long[] bufferInternal = new long[1];
		bufferInternal[0] = 0b010101111010;
		Buffer buffer = new Buffer(TEST_BUFFER_ID, bufferInternal, Long.SIZE * 2);

		// Act

		// Assert
		assertEquals("0000000000000000000000000000000000000000000000000000010101111010|", buffer.toString());
		assertEquals(buffer.getBufferId(), TEST_BUFFER_ID);
	}

	@Test
	public void whenWriteBooleanTrue() {
		// Arrange
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor();
		long expectBuffer = 1l << 63;
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(expectBuffer));

		// Act
		buffer.appendBit(cursor, true);

		// Assert
		UtilsTest.assertEqualsBitLong(expectBuffer, buffer.getBufferArray()[0]);
		assertEquals(INITIAL_SIZE, buffer.getBufferArray().length);
		assertEquals(1, buffer.getBitSize());
		assertEquals(Long.SIZE - 2l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
		assertEquals(1, cursor.getBitCursor());
	}

	@Test
	public void whenWriteBooleanFalse() {
		// Arrange
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor();
		long expectBuffer = 0l;
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(expectBuffer));

		// Act
		buffer.appendBit(cursor, false);

		// Assert
		UtilsTest.assertEqualsBitLong(expectBuffer, buffer.getBufferArray()[0]);
		assertEquals(INITIAL_SIZE, buffer.getBufferArray().length);
		assertEquals(1, buffer.getBitSize());
		assertEquals(Long.SIZE - 2l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
		assertEquals(1, cursor.getBitCursor());
	}

	@Test
	public void whenWriteBooleanOnNextLong() {
		// Arrange
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(Long.SIZE - 1, 0, 0);
		long expectBuffer01 = 1l;
		long expectBuffer02 = 1l << 63;
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendBit(cursor, true);
		buffer.appendBit(cursor, true);

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[1]);
		assertEquals(INITIAL_SIZE, buffer.getBufferArray().length);
		assertEquals(2, buffer.getBitSize());
		assertEquals(Long.SIZE - 2l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 1l, cursor.getBitCursor());
	}

	@Test
	public void whenWriteBooleanAndGrowBuffer() {
		// Arrange
		long expectBuffer = 1l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(INITIAL_SIZE * Long.SIZE, 0, INITIAL_SIZE - 1);
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(expectBuffer));

		// Act
		buffer.appendBit(cursor, true);

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER_FORMAT, expectBuffer, buffer.getBufferArray()[INITIAL_SIZE - 1]);
		assertEquals(INITIAL_SIZE * 2l, buffer.getBufferArray().length);
		assertEquals(Cursor.CURSOR_START_POS, cursor.getBitInLongCursor());
		assertEquals(INITIAL_SIZE, cursor.getLongCursor());
	}

	@Test
	public void whenWriteLong() {
		// Arrange
		long expectBuffer = 0b1010101l << 57;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor();
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(expectBuffer));

		// Act
		buffer.appendLong(cursor, 0b1010101l, 7);

		// Assert
		UtilsTest.assertEqualsBitLong(expectBuffer, buffer.getBufferArray()[0]);
		assertEquals(INITIAL_SIZE, buffer.getBufferArray().length);
		assertEquals(Long.SIZE - 8l, cursor.getBitInLongCursor());
		assertEquals(0, cursor.getLongCursor());
	}

	@Test()
	public void whenWriteLongWithNegativeSize() {
		// Arrange
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor();

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.appendLong(cursor, 0b1010101l, -1);
		});

		// Assert
	}

	@Test()
	public void whenWriteLongWithOverSize() {
		// Arrange
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor();

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			buffer.appendLong(cursor, 0b1010101l, Long.SIZE + 1);
		});

		// Assert
	}

	@Test
	public void whenWriteLongOnNextLong() {
		// Arrange
		long expectBuffer01 = 0b11001l;
		long expectBuffer02 = 0b10001l << 59;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(Long.SIZE - 5, 4, 0);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendLong(cursor, 0b1100110001l, 10);

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[1]);
		assertEquals(Long.SIZE - 6l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 5l, cursor.getBitCursor());
		assertEquals(10, buffer.getBitSize());
	}

	@Test
	public void whenWriteLongOnNextLongAndAddBoolean() {
		// Arrange
		long expectBuffer01 = 0b11001l;
		long expectBuffer02 = 0b100011l << 58;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(Long.SIZE - 5, 4, 0);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendLong(cursor, 0b1100110001l, 10);
		buffer.appendBit(cursor, true);

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[1]);
		assertEquals(Long.SIZE - 7l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
	}

	@Test
	public void whenWriteLongOnNextLongAndAddLong() {
		// Arrange
		long expectBuffer01 = 0b11001l;
		long expectBuffer02 = 0b1000111111l << 54;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(Long.SIZE - 5, 4, 0);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendLong(cursor, 0b1100110001l, 10);
		buffer.appendLong(cursor, 0b11111l, 5);

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[1]);
		assertEquals(Long.SIZE - 11l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
	}

	@Test
	public void whenWriteLongAndGrowBuffer() {
		// Arrange
		long expectBuffer = 0b10101l;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(INITIAL_SIZE * Long.SIZE - 5, 4, INITIAL_SIZE - 1);
		LOG.debug(EXPECT_BUFFER_FORMAT, UtilsTest.byteFormatLong(expectBuffer));

		// Act
		buffer.appendLong(cursor, 0b10101l, 5);

		// Assert
		UtilsTest.assertEqualsBitLong(expectBuffer, buffer.getBufferArray()[INITIAL_SIZE - 1]);
		assertEquals(INITIAL_SIZE * 2l, buffer.getBufferArray().length);
		assertEquals(Cursor.CURSOR_START_POS, cursor.getBitInLongCursor());
		assertEquals(INITIAL_SIZE, cursor.getLongCursor());
	}

	@Test
	public void whenWriteLongOnNextLongAndGrowBuffer() {
		// Arrange
		long expectBuffer01 = 0b11001l;
		long expectBuffer02 = 0b10001l << 59;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(INITIAL_SIZE * Long.SIZE - 5, 4, INITIAL_SIZE - 1);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendLong(cursor, 0b1100110001l, 10);

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[INITIAL_SIZE - 1]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[INITIAL_SIZE]);
		assertEquals(INITIAL_SIZE * 2l, buffer.getBufferArray().length);
		assertEquals(Long.SIZE - 6l, cursor.getBitInLongCursor());
		assertEquals(INITIAL_SIZE, cursor.getLongCursor());
	}

	@Test
	public void whenBufferAreSort() {
		// Arrange
		UUID uuid = UUID.fromString("123-123-123-123-123");
		Buffer buffer1 = new Buffer(new BufferId(uuid, 1));
		Buffer buffer2 = new Buffer(new BufferId(uuid, 2));
		Buffer buffer3 = new Buffer(new BufferId(uuid, 3));
		List<Buffer> actual = Arrays.asList(buffer2, buffer1, buffer3);
		List<Buffer> expected = Arrays.asList(buffer1, buffer2, buffer3);

		// Act
		actual.sort(Buffer.COMPARATOR_BY_TIME);

		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void whenWriteBytes() {
		// Arrange
		long expectBuffer01 = 0b1001_1101_1100_0011l << 48;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor();
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01));

		// Act
		buffer.appendBytes(cursor, new byte[] { (byte) (0xFF & 0b1001_1101), (byte) (0xFF & 0b1100_0011) });

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
	}

	@Test
	public void whenWriteByteOnNextLong() {
		// Arrange
		long expectBuffer01 = 0b1001l;
		long expectBuffer02 = 0b1101l << 60;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(Long.SIZE - 4, 3, 0);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendByte(cursor, (byte) (0xFF & 0b1001_1101));

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[1]);
		assertEquals(Long.SIZE - 5l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 4l, cursor.getBitCursor());
		assertEquals(8, buffer.getBitSize());
	}

	@Test
	public void whenWriteByteOnNextLongAndAddBoolean() {
		long expectBuffer01 = 0b1001l;
		long expectBuffer02 = 0b11011l << 59;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(Long.SIZE - 4, 3, 0);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendByte(cursor, (byte) (0xFF & 0b1001_1101));
		buffer.appendBit(cursor, true);

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[1]);
		assertEquals(Long.SIZE - 6l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
		assertEquals(Long.SIZE + 5l, cursor.getBitCursor());
		assertEquals(9, buffer.getBitSize());
	}

	@Test
	public void whenWriteByteOnNextByteAndAddByte() {
		// Arrange
		long expectBuffer01 = 0b1001l;
		long expectBuffer02 = 0b1101_1100_0011l << 52;
		Buffer buffer = new Buffer(TEST_BUFFER_ID);
		Cursor cursor = new Cursor(Long.SIZE - 4, 3, 0);
		LOG.debug(PRINT_DOUBLE_BUFFER, UtilsTest.byteFormatLong(expectBuffer01), UtilsTest.byteFormatLong(expectBuffer02));

		// Act
		buffer.appendByte(cursor, (byte) (0xFF & 0b1001_1101));
		buffer.appendByte(cursor, (byte) (0xFF & 0b1100_0011));

		// Assert
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER01, expectBuffer01, buffer.getBufferArray()[0]);
		UtilsTest.assertEqualsBitLong(EXPECT_BUFFER02, expectBuffer02, buffer.getBufferArray()[1]);
		assertEquals(Long.SIZE - 13l, cursor.getBitInLongCursor());
		assertEquals(1, cursor.getLongCursor());
	}

}
