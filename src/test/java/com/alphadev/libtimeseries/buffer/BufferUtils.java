package com.alphadev.libtimeseries.buffer;

import java.util.UUID;

public class BufferUtils {
	private static final BufferId TEST_BUFFER_ID = new BufferId(UUID.fromString("123-123-123-123-123"), 1);

	private BufferUtils() {}

	public static BufferId getTestBufferId() {
		return TEST_BUFFER_ID;
	}

	public static Buffer getTestBuffer() {
		return new Buffer(TEST_BUFFER_ID);
	}

	public static Buffer getTestBuffer(BufferId bufferId) {
		return new Buffer(bufferId);
	}

	public static Buffer getTestBadBuffer() {
		return new Buffer(new BufferId(UUID.fromString("000-000-000-000-000"), 0));
	}

	public static Buffer getTestBuffer(long[] bufferArray, int bitSize) {
		return new Buffer(TEST_BUFFER_ID, bufferArray, bitSize);
	}

	public static Buffer getTestBuffer(BufferId bufferId, long[] bufferArray, int bitSize) {
		return new Buffer(bufferId, bufferArray, bitSize);
	}

	public static Cursor getCursor(int bitCursor, int bitInLongCursor, int longCursor) {
		return new Cursor(bitCursor, bitInLongCursor, longCursor);
	}

	public static Cursor getTestCursor() {
		return new Cursor();
	}

}
