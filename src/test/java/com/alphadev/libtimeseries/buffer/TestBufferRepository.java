package com.alphadev.libtimeseries.buffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.LongBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.alphadev.libtimeseries.LibTimeSeries;
import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.serie.ToWriteBuffer;
import com.alphadev.libtimeseries.synchronizer.SynchronizerRepository;
import com.alphadev.libtimeseries.synchronizer.SynchronizerType;

public class TestBufferRepository {
	private static final String DB_NAME = "testing";

	@BeforeEach
	public void setUp() {
		LibTimeSeries.load(DB_NAME, SynchronizerType.MEMORY);
	}

	@AfterEach
	public void tearDown() {
		LibTimeSeries.close();
	}

	@Test
	public void testMakeNewBuffer() {
		// Arrange
		BufferId bufferId = BufferUtils.getTestBufferId();

		// Act
		Buffer buffer = BufferRepository.makeNewBuffer(bufferId);

		// Assert
		Optional<Buffer> loadBuffer = BufferRepository.getLoadBuffer(bufferId);
		assertEquals(buffer, loadBuffer.orElse(null));
		assertEquals(buffer.getBufferId(), bufferId);

	}

	@Test()
	public void testMakeNewBufferFailWithAlreadyExistingBuifferId() {
		// Arrange
		BufferId bufferId = BufferUtils.getTestBufferId();
		BufferRepository.makeNewBuffer(bufferId);

		// Act
		assertThrows(TimeSeriesException.class, () -> {
			BufferRepository.makeNewBuffer(bufferId);
		});

		// Assert
	}

	@Test
	public void testLoadBuffer() {
		// Arrange
		BufferId bufferId = BufferUtils.getTestBufferId();
		int totalSize = Long.SIZE + Long.SIZE + Long.SIZE;
		long[] toLoad = { 10l, 20l, 30l };
		SynchronizerRepository.writeBuffer(new ToWriteBuffer(new Buffer(bufferId, toLoad, totalSize), new Cursor()));
		BufferRepository.load();

		// Act
		Buffer buffer = BufferRepository.makeBuffer(bufferId, toLoad, totalSize);

		// Assert
		assertEquals(buffer.getBufferId(), bufferId);
		assertEquals(buffer.getBitSize(), totalSize);
		assertEquals(buffer.getBufferArray(), toLoad);
	}

	@Test
	public void testGetBufferNoExist() {
		// Arrange
		BufferId bufferId = BufferUtils.getTestBufferId();

		// Act
		Optional<Buffer> actual = BufferRepository.getLoadBuffer(bufferId);

		// Assert
		assertFalse(actual.isPresent());
	}

	@Test
	public void testFindBuffers() {
		// Arrange
		UUID serieUUID = UUID.fromString("123-123-123-123-123");
		BufferId bufferId01 = new BufferId(serieUUID, 1);
		BufferId bufferId02 = new BufferId(serieUUID, 2);
		BufferId bufferId03 = new BufferId(serieUUID, 3);
		BufferId bufferId04 = new BufferId(serieUUID, 4);
		BufferId bufferId05 = new BufferId(UUID.fromString("aaa-aaa-aaa-aaa-aaa"), 3);

		LongBuffer buffer = LongBuffer.allocate(3);
		buffer.put(bufferId04.getRefTime());
		buffer.put(0b111000111001010110000000l);
		buffer.flip();

		int totalSize = Long.SIZE + 17;
		SynchronizerRepository.writeBuffer(new ToWriteBuffer(new Buffer(bufferId04, buffer.array(), totalSize), new Cursor()));
		BufferRepository.load();

		Buffer buffer1 = BufferRepository.makeNewBuffer(bufferId01);
		Buffer buffer2 = BufferRepository.makeNewBuffer(bufferId02);
		Buffer buffer3 = BufferRepository.makeNewBuffer(bufferId03);
		BufferRepository.makeNewBuffer(bufferId05);

		// Act
		List<Buffer> actual = BufferRepository.findBuffers(serieUUID, 2l, 4l);

		// Assert
		List<Buffer> expected = Arrays.asList(buffer1, buffer2, buffer3, BufferRepository.getLoadBuffer(bufferId04).orElse(null));
		assertEquals(expected, actual);
	}

	@Test
	public void testDeleteBuffers() {
		// Arrange
		UUID serieUUID = UUID.fromString("123-123-123-123-123");
		BufferId bufferId01 = new BufferId(serieUUID, 1);
		BufferId bufferId02 = new BufferId(serieUUID, 2);
		BufferId bufferId03 = new BufferId(serieUUID, 3);
		BufferId bufferId04 = new BufferId(serieUUID, 4);
		BufferId bufferId05 = new BufferId(UUID.fromString("aaa-aaa-aaa-aaa-aaa"), 3);

		LongBuffer buffer = LongBuffer.allocate(3);
		buffer.put(bufferId04.getRefTime());
		buffer.put(0b111000111001010110000000l);
		buffer.flip();

		int totalSize = Long.SIZE + 17;
		SynchronizerRepository.writeBuffer(new ToWriteBuffer(new Buffer(bufferId04, buffer.array(), totalSize), new Cursor()));
		BufferRepository.load();

		BufferRepository.makeNewBuffer(bufferId01);
		BufferRepository.makeNewBuffer(bufferId02);
		BufferRepository.makeNewBuffer(bufferId03);
		BufferRepository.makeNewBuffer(bufferId05);

		// Act
		BufferRepository.deleteBuffers(serieUUID);
		List<Buffer> actual = BufferRepository.findBuffers(serieUUID, 2l, 4l);

		// Assert
		assertEquals(0, actual.size());
	}
}
