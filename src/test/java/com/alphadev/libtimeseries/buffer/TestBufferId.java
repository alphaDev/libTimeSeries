package com.alphadev.libtimeseries.buffer;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.ByteBuffer;
import java.util.UUID;

import org.junit.jupiter.api.Test;

public class TestBufferId {

	private static final UUID UUID_123 = UUID.fromString("123-123-123-123-123");

	@Test
	public void whenBufferIdEqualsWithNull() {
		// Arrange
		BufferId bufferId = BufferUtils.getTestBufferId();

		// Act
		boolean actual = bufferId.equals(null);

		// Assert
		assertFalse(actual);
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void whenBufferIdEqualsWithString() {
		// Arrange
		BufferId bufferId = BufferUtils.getTestBufferId();

		// Act
		boolean actual = bufferId.equals("toto");

		// Assert
		assertFalse(actual);
	}

	@Test
	public void whenBufferIdEqualsWithSameInstance() {
		// Arrange
		BufferId bufferId = BufferUtils.getTestBufferId();

		// Act
		boolean actual = bufferId.equals(bufferId);

		// Assert
		assertTrue(actual);
	}

	@Test
	public void whenBufferIdEqualsWithDiffId() {
		// Arrange
		BufferId bufferId1 = new BufferId(UUID_123, 1);
		BufferId bufferId2 = new BufferId(UUID.fromString("123-123-123-123-124"), 1);

		// Act
		boolean actual = bufferId1.equals(bufferId2);

		// Assert
		assertFalse(actual);
	}

	@Test
	public void whenBufferIdEqualsWithDiffRefTime() {
		// Arrange
		BufferId bufferId1 = new BufferId(UUID_123, 1);
		BufferId bufferId2 = new BufferId(UUID_123, 2);

		// Act
		boolean actual = bufferId1.equals(bufferId2);

		// Assert
		assertFalse(actual);
	}

	@Test
	public void whenBufferIdEqualsWithSameItem() {
		// Arrange
		BufferId bufferId1 = new BufferId(UUID_123, 1);
		BufferId bufferId2 = new BufferId(UUID_123, 1);

		// Act
		boolean actual = bufferId1.equals(bufferId2);

		// Assert
		assertTrue(actual);
	}

	@Test
	public void whenBufferIdSerialize() {
		// Arrange
		BufferId bufferId = new BufferId(UUID_123, 1);

		ByteBuffer expected = ByteBuffer.allocate(Long.BYTES * 3);
		expected.putLong(UUID_123.getMostSignificantBits());
		expected.putLong(UUID_123.getLeastSignificantBits());
		expected.putLong(1);

		// Act
		byte[] actual = bufferId.serialize();

		// Assert
		assertArrayEquals(expected.array(), actual);
	}

	@Test
	public void whenBufferIdUnserialize() {
		// Arrange
		BufferId bufferId = new BufferId(UUID_123, 1);

		ByteBuffer rawBufferId = ByteBuffer.allocate(Long.BYTES * 3);
		rawBufferId.putLong(UUID_123.getMostSignificantBits());
		rawBufferId.putLong(UUID_123.getLeastSignificantBits());
		rawBufferId.putLong(1);

		// Act
		BufferId actual = BufferId.unserialize(rawBufferId.array());

		// Assert
		assertEquals(bufferId, actual);
	}

}
