package com.alphadev.libtimeseries.synchronizer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.rocksdb.RocksDBException;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.buffer.BufferUtils;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.Metadata;
import com.alphadev.libtimeseries.serie.Serie;
import com.alphadev.libtimeseries.serie.ToWriteBuffer;
import com.alphadev.libtimeseries.serie.ValueFormat;
import com.alphadev.libtimeseries.serie.entry.Entry;
import com.alphadev.libtimeseries.serie.entry.LongEntry;

/**
 * wait for rocksdbjni version 5.14.*
 */
@Disabled
public class TestRocksDbSynchronizer {
	private static final Logger					LOG					= LogManager.getLogger(TestRocksDbSynchronizer.class);
	private static final String					NAME				= "test";
	private static final Path					DB_PATH				= Paths.get(NAME + "-lib-ts");
	private static final RocksDbSynchronizer	rocksDbSynchronizer	= new RocksDbSynchronizer(NAME);

	@AfterEach
	public void tearDown() throws IOException, InterruptedException {
		try {
			rocksDbSynchronizer.close();
		}
		catch (NullPointerException e) {}

		if (Files.exists(DB_PATH)) {
			Files.walk(DB_PATH).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
		}
	}

	@Test
	public void openclose() throws RocksDBException, IOException {
		// Arrange
		rocksDbSynchronizer.load();
		rocksDbSynchronizer.close();

		// Act
		rocksDbSynchronizer.load();

		// Assert
		int totalSize = Long.SIZE + Long.SIZE + Long.SIZE;
		long[] toLoad = { 10l, 20l, 30l };
		BufferId bufferId01 = new BufferId(UUID.fromString("123-123-123-123-001"), 1);
		Buffer buffer01 = BufferUtils.getTestBuffer(bufferId01, toLoad, totalSize);
		ToWriteBuffer toWriteBuffer01 = new ToWriteBuffer(buffer01, new Cursor());

		toWriteBuffer01 = new ToWriteBuffer(buffer01, new Cursor());

		rocksDbSynchronizer.writeBuffer(toWriteBuffer01);
	}

	@Test
	public void whenLoadOK() {
		// Arrange

		// Act
		rocksDbSynchronizer.load();

		// Assert
		assertTrue(Files.exists(DB_PATH));
	}

	@Test
	public void whenSaveSerie() {
		// Arrange
		rocksDbSynchronizer.load();
		UUID id = UUID.fromString("123-123-123-123-123");
		Metadata metadata = new Metadata(Arrays.asList("trag01"), "ms", ValueFormat.LONG);
		Serie<LongEntry, Long> serie = new Serie<>(id, metadata);

		// Act
		rocksDbSynchronizer.saveSerie(serie);

		// Assert
		Collection<Serie<? extends Entry<?>, ?>> series = rocksDbSynchronizer.getKnownSeries();
		LOG.debug("series [{}]", series);
		assertTrue(series.contains(serie));
	}

	@Test
	public void whenDeleteSerieSerie() {
		// Arrange
		rocksDbSynchronizer.load();
		UUID id01 = UUID.fromString("123-123-123-123-001");
		UUID id02 = UUID.fromString("123-123-123-123-002");
		Metadata metadata01 = new Metadata(Arrays.asList("test01"), "ms", ValueFormat.LONG);
		Metadata metadata02 = new Metadata(Arrays.asList("test02"), "ms", ValueFormat.LONG);
		Serie<LongEntry, Long> serie01 = new Serie<>(id01, metadata01);
		Serie<LongEntry, Long> serie02 = new Serie<>(id02, metadata02);
		rocksDbSynchronizer.saveSerie(serie01);
		rocksDbSynchronizer.saveSerie(serie02);
		Collection<Serie<? extends Entry<?>, ?>> series = rocksDbSynchronizer.getKnownSeries();
		assertEquals(Arrays.asList(serie01, serie02), series);

		// Act
		rocksDbSynchronizer.deleteSerie(serie01);

		// Assert
		Collection<Serie<? extends Entry<?>, ?>> series2 = rocksDbSynchronizer.getKnownSeries();
		assertEquals(Arrays.asList(serie02), series2);
	}

	@Test
	public void whenSaveBuffer() {
		// Arrange
		rocksDbSynchronizer.load();
		int totalSize = Long.SIZE + Long.SIZE + Long.SIZE;
		long[] toLoad = { 10l, 20l, 30l };
		final BufferId bufferId01 = new BufferId(UUID.fromString("123-123-123-123-001"), 1);
		final BufferId bufferId02 = new BufferId(UUID.fromString("123-123-123-123-002"), 1);
		final BufferId bufferId03 = new BufferId(UUID.fromString("123-123-123-123-003"), 1);

		Buffer buffer01 = BufferUtils.getTestBuffer(bufferId01, toLoad, totalSize);
		Buffer buffer02 = BufferUtils.getTestBuffer(bufferId02, toLoad, totalSize);
		Buffer buffer03 = BufferUtils.getTestBuffer(bufferId03, toLoad, totalSize);

		ToWriteBuffer toWriteBuffer01 = new ToWriteBuffer(buffer01, new Cursor());
		ToWriteBuffer toWriteBuffer02 = new ToWriteBuffer(buffer02, new Cursor());
		ToWriteBuffer toWriteBuffer03 = new ToWriteBuffer(buffer03, new Cursor());

		// Act
		rocksDbSynchronizer.writeBuffer(toWriteBuffer01);
		rocksDbSynchronizer.writeBuffer(toWriteBuffer02);
		rocksDbSynchronizer.writeBuffer(toWriteBuffer03);

		// Assert
		Collection<BufferId> buffers = rocksDbSynchronizer.getKnownBuffers();
		assertEquals(Arrays.asList(bufferId01, bufferId02, bufferId03), buffers);
	}

	@Test
	public void whenDeleteBuffer() {
		// Arrange
		rocksDbSynchronizer.load();
		int totalSize = Long.SIZE + Long.SIZE + Long.SIZE;
		long[] toLoad = { 10l, 20l, 30l };
		final BufferId bufferId01 = new BufferId(UUID.fromString("123-123-123-123-001"), 1);
		final BufferId bufferId02 = new BufferId(UUID.fromString("123-123-123-123-002"), 1);
		final BufferId bufferId03 = new BufferId(UUID.fromString("123-123-123-123-003"), 1);

		Buffer buffer01 = BufferUtils.getTestBuffer(bufferId01, toLoad, totalSize);
		Buffer buffer02 = BufferUtils.getTestBuffer(bufferId02, toLoad, totalSize);
		Buffer buffer03 = BufferUtils.getTestBuffer(bufferId03, toLoad, totalSize);

		ToWriteBuffer toWriteBuffer01 = new ToWriteBuffer(buffer01, new Cursor());
		ToWriteBuffer toWriteBuffer02 = new ToWriteBuffer(buffer02, new Cursor());
		ToWriteBuffer toWriteBuffer03 = new ToWriteBuffer(buffer03, new Cursor());

		rocksDbSynchronizer.writeBuffer(toWriteBuffer01);
		rocksDbSynchronizer.writeBuffer(toWriteBuffer02);
		rocksDbSynchronizer.writeBuffer(toWriteBuffer03);

		Collection<BufferId> buffers = rocksDbSynchronizer.getKnownBuffers();
		assertEquals(Arrays.asList(bufferId01, bufferId02, bufferId03), buffers);

		// Act
		rocksDbSynchronizer.deleteBuffers(Arrays.asList(bufferId01, bufferId03));

		// Assert
		Collection<BufferId> buffers2 = rocksDbSynchronizer.getKnownBuffers();
		assertEquals(Arrays.asList(bufferId02), buffers2);
	}

}
