package com.alphadev.libtimeseries.benchmark;

import java.io.IOException;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.RunnerException;

import com.alphadev.libtimeseries.LibTimeSeries;
import com.alphadev.libtimeseries.serie.ValueFormat;
import com.alphadev.libtimeseries.serie.entry.LongEntry;
import com.alphadev.libtimeseries.synchronizer.SynchronizerType;

@Fork(value = 3)
@Warmup(iterations = 5)
@Measurement(iterations = 10)
@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class BenchmarkLibTimeSeries {

	long	time			= 1523963343l;
	long[]	valuesVariation	= new long[] { 1l, 1l, 2l, 3l };
	long[]	timeVariation	= new long[] { 1l, 1l, 2l, 3l };
	int		counter			= 0;

	@Setup
	public void setUp() {
		LibTimeSeries.load("test", SynchronizerType.ROCKSDB);
		LibTimeSeries.delete("TimeFixValueFix");
		LibTimeSeries.delete("TimeFixValueVariation");
		LibTimeSeries.delete("TimeVariationValueFix");
		LibTimeSeries.delete("TimeVariationValueVariation");
		LibTimeSeries.createOrUpdateSerie(ValueFormat.LONG, "", "TimeFixValueFix");
		LibTimeSeries.createOrUpdateSerie(ValueFormat.LONG, "", "TimeFixValueVariation");
		LibTimeSeries.createOrUpdateSerie(ValueFormat.LONG, "", "TimeVariationValueFix");
		LibTimeSeries.createOrUpdateSerie(ValueFormat.LONG, "", "TimeVariationValueVariation");
	}

	@TearDown
	public void tearDown() {
		LibTimeSeries.close();
	}

	public LongEntry getNextLongEntryTimeFixValueFix() {
		counter++;
		return new LongEntry(time + counter, 5);
	}

	public LongEntry getNextLongEntryTimeFixValueVariation() {
		counter++;
		return new LongEntry(time + counter, valuesVariation[counter % valuesVariation.length]);
	}

	public LongEntry getNextLongEntryTimeVariationValueFix() {
		counter++;
		return new LongEntry(time + counter + timeVariation[counter % timeVariation.length], 5);
	}

	public LongEntry getNextLongEntryTimeVariationValueVariation() {
		counter++;
		return new LongEntry(time + counter + timeVariation[counter % timeVariation.length], valuesVariation[counter % valuesVariation.length]);
	}

	@org.openjdk.jmh.annotations.Benchmark
	public void writeWithTimeFixValueFix(BenchmarkLibTimeSeries plan) throws InterruptedException {
		LongEntry le = plan.getNextLongEntryTimeFixValueFix();
		LibTimeSeries.add(le.getTimestamp(), le.getValue(), "TimeFixValueFix");
	}

	@org.openjdk.jmh.annotations.Benchmark
	public void writeWithTimeFixValueVariation(BenchmarkLibTimeSeries plan) throws InterruptedException {
		LongEntry le = plan.getNextLongEntryTimeFixValueVariation();
		LibTimeSeries.add(le.getTimestamp(), le.getValue(), "TimeFixValueVariation");
	}

	@org.openjdk.jmh.annotations.Benchmark
	public void writeWithTimeVariationValueFix(BenchmarkLibTimeSeries plan) throws InterruptedException {
		LongEntry le = plan.getNextLongEntryTimeVariationValueFix();
		LibTimeSeries.add(le.getTimestamp(), le.getValue(), "TimeVariationValueFix");
	}

	@org.openjdk.jmh.annotations.Benchmark
	public void writeWithTimeVariationValueVariation(BenchmarkLibTimeSeries plan) throws InterruptedException {
		LongEntry le = plan.getNextLongEntryTimeVariationValueVariation();
		LibTimeSeries.add(le.getTimestamp(), le.getValue(), "TimeVariationValueVariation");
	}

	public static void main(String[] args) throws RunnerException, IOException {
		org.openjdk.jmh.Main.main(args);
	}
}
