package com.alphadev.libtimeseries.serie;

import java.io.Serializable;

public enum ValueFormat implements Serializable {
	LONG, FLOAT, FIXED_POINT, STRING
}
