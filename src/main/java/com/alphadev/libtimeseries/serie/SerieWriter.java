package com.alphadev.libtimeseries.serie;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.buffer.BufferRepository;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.driver.Encoder;
import com.alphadev.libtimeseries.serie.entry.Entry;
import com.alphadev.libtimeseries.synchronizer.SynchronizerRepository;

public class SerieWriter<T extends Entry<H>, H> {
	private static final Logger				LOG						= LogManager.getLogger(SerieWriter.class);

	public static final int					MAX_DURATION			= 3600;															// 1h
	protected static final int				DEFAULT_MAX_SIZE		= 8190 * Long.SIZE;												// 64ko
	protected static final int				DEFAULT_MAX_DURATION	= MAX_DURATION;
	private static final ExecutorService	EXECUTOR				= Executors.newFixedThreadPool(8, r -> {
																		Thread t = Executors.defaultThreadFactory().newThread(r);
																		t.setDaemon(true);
																		return t;
																	});

	private static long						maxDuration				= DEFAULT_MAX_DURATION;
	private static long						maxSize					= DEFAULT_MAX_SIZE;

	private UUID							id;
	private boolean							writeFirstEntry			= true;
	private boolean							writeRefTime			= true;
	private Integer							refDiffTime				= null;
	private T								lastEntry;
	private Buffer							buffer;
	private Cursor							cursor;
	private long							firstEntryTimestamp;

	private ToWriteBuffer					toWriteBuffer;
	private Future<Boolean>					saveBufferFileDone;

	private Encoder<T, H>					entryEncoder;

	public SerieWriter(UUID id, Encoder<T, H> encoder) {
		this.id = id;
		this.entryEncoder = encoder;
	}

	public SerieWriter(UUID id, int refDiffTime, Encoder<T, H> encoder) {
		this.id = id;
		this.refDiffTime = refDiffTime;
		this.entryEncoder = encoder;
	}

	public void add(T entry) {
		// LOG.debug("add entry:[{}]", entry);
		if (writeFirstEntry) {
			buffer = BufferRepository.makeNewBuffer(new BufferId(id, entry.getTimestamp()));
			cursor = new Cursor();

			entryEncoder.encodeInitialEntry(entry, buffer, cursor);
			firstEntryTimestamp = entry.getTimestamp();
			lastEntry = entry;
			writeFirstEntry = false;
			return;
		}

		if (writeRefTime) {
			if (refDiffTime == null) {
				refDiffTime = (int) (entry.getTimestamp() - lastEntry.getTimestamp());
			}
			entryEncoder.encodeRefDiffTime(refDiffTime, buffer, cursor);
			writeRefTime = false;
		}

		entryEncoder.encodeNextEntry(lastEntry, entry, refDiffTime, buffer, cursor);
		lastEntry = entry;

		if ((entry.getTimestamp() - firstEntryTimestamp >= maxDuration) || cursor.getBitCursor() >= maxSize) {
//			LOG.debug("must call saveBuffer on duration [{}] maxDuration:[{}] firstEntryTimestamp:[{}] entry:[{}]", (entry.getTimestamp() - firstEntryTimestamp >= maxDuration), maxDuration, firstEntryTimestamp, entry.getTimestamp());
//			LOG.debug("must call saveBuffer on buffer size:[{}] getBitCursor [{}] maxSize:[{}]", (cursor.getBitCursor() >= maxSize), cursor.getBitCursor(), maxSize);
			saveBuffer();
		}
	}

	public static void setMaxDuration(long maxDuration) {
		SerieWriter.maxDuration = Math.min(maxDuration, MAX_DURATION);
	}

	public static void setMaxSize(long maxSize) {
		SerieWriter.maxSize = maxSize;
	}

	protected void saveBuffer() {
		// LOG.debug("call saveBuffer");
		if (saveBufferFileDone != null) {
			try {
//				long start = System.currentTimeMillis();
				saveBufferFileDone.get();
//				LOG.debug("wait for saving buffer:[{}]ms", (System.currentTimeMillis() - start));
			}
			catch (InterruptedException | ExecutionException exp) {
				LOG.error(exp);
				throw new TimeSeriesException(exp);
			}
		}

		prepareToWrite();
		saveBufferFileDone = EXECUTOR.submit(this::saveBufferFile);
	}

	protected Boolean saveBufferFile() {
		try {
			callWriteBuffer(toWriteBuffer);
		}
		catch (Exception e) {
			LOG.error(e);
			throw new TimeSeriesException(e);
		}
		finally {
			toWriteBuffer = null;
		}

		return Boolean.TRUE;
	}

	private void prepareToWrite() {
		toWriteBuffer = new ToWriteBuffer(buffer, cursor);
		writeFirstEntry = true;
		writeRefTime = true;
		lastEntry = null;
		firstEntryTimestamp = 0;
	}

	protected void callWriteBuffer(ToWriteBuffer toWriteBuffer) {
//		LOG.debug("add callWriteBuffer");
		SynchronizerRepository.writeBuffer(toWriteBuffer);
	}

}
