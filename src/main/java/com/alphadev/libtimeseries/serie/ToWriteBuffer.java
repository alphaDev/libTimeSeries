package com.alphadev.libtimeseries.serie;

import java.nio.ByteBuffer;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.buffer.Cursor;

public class ToWriteBuffer {

	private final ByteBuffer	buffer;
	private final int			sizeBits;
	private final BufferId		buffeId;

	public ToWriteBuffer(Buffer toWritebuffer, Cursor cursor) {
		sizeBits = cursor.getBitCursor();
		buffer = ByteBuffer.allocate((cursor.getLongCursor() + 1) * Long.BYTES + Integer.BYTES);
		buffer.putInt(sizeBits);
		for (int i = 0; i < cursor.getLongCursor() + 1; i++) {
			buffer.putLong(toWritebuffer.getBufferArray()[i]);
		}
		buffer.flip();
		buffer.limit((int) Math.ceil((float) (sizeBits + Integer.SIZE) / Byte.SIZE));
		buffeId = toWritebuffer.getBufferId();
	}

	public BufferId getBuffeId() {
		return buffeId;
	}

	public ByteBuffer getBuffer() {
		return buffer;
	}

	public int getSizeBits() {
		return sizeBits;
	}

}
