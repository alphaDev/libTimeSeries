package com.alphadev.libtimeseries.serie;

import static com.googlecode.cqengine.query.QueryFactory.equal;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.buffer.BufferRepository;
import com.alphadev.libtimeseries.serie.entry.Entry;
import com.alphadev.libtimeseries.serie.entry.LongRange;
import com.alphadev.libtimeseries.synchronizer.SynchronizerRepository;
import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.index.unique.UniqueIndex;
import com.googlecode.cqengine.resultset.common.NoSuchObjectException;
import com.googlecode.cqengine.resultset.common.NonUniqueObjectException;

public class SerieRepository {
	private static final Logger												LOG		= LogManager.getLogger(SerieRepository.class);
	private static final IndexedCollection<Serie<? extends Entry<?>, ?>>	SERIES	= new ConcurrentIndexedCollection<>();

	private SerieRepository() {}

	public static void load() {
		SERIES.addAll(SynchronizerRepository.getKnownSeries());
		try {
			SERIES.addIndex(UniqueIndex.onAttribute(Serie.TAG_ID));
		}
		catch (IllegalStateException e) {
			LOG.debug("Skip index creation. They were already made");
		}
	}

	public static void close() {
		SERIES.clear();
	}

	public static <T extends Entry<H>, H> void createOrUpdateSerie(Metadata metadata) {
		Optional<Serie<T, H>> oSerie = findSerie(metadata.getTags());
		if (oSerie.isPresent()) {
			oSerie.get().setMetadata(metadata);
		}
		else {
			addNewSerie(metadata);
		}
	}

	public static <T extends Entry<H>, H> void add(List<String> tags, T entry) {
		Optional<Serie<T, H>> oSerie = findSerie(tags);
		Serie<T, H> serie = oSerie.orElseThrow(() -> new TimeSeriesException("Unknown serie with tag: [" + String.join(",", tags) + "]"));
		serie.add(entry);
	}

	public static <T extends Entry<H>, H> List<T> read(List<String> tags, long start, long end) {
		Optional<Serie<T, H>> oSerie = findSerie(tags);
		Serie<T, H> serie = oSerie.orElseThrow(() -> new TimeSeriesException("Unknown serie with tag: [" + String.join(",", tags) + "]"));
		return serie.read(start, end);
	}

	public static List<LongRange> readRange(List<String> tags, long start, long end, long margingCreateNewRange) {
		Optional<Serie<LongRange, Long>> oSerie = findSerie(tags);
		Serie<LongRange, Long> serie = oSerie.orElseThrow(() -> new TimeSeriesException("Unknown serie with tag: [" + String.join(",", tags) + "]"));
		return serie.readRange(start, end, margingCreateNewRange);
	}

	public static Set<List<String>> listExisingSerie() {
		return SERIES.stream().map(s -> s.getMetadata().getTags()).collect(Collectors.toSet());
	}

	public static <T extends Entry<H>, H> void delete(List<String> tags) {
		Optional<Serie<T, H>> oSerie = findSerie(tags);
		if (oSerie.isPresent()) {
			final Serie<T, H> serie = oSerie.get();
			SERIES.remove(serie);
			SynchronizerRepository.deleteSerie(serie);
			BufferRepository.deleteBuffers(serie.getId());
		}
	}

	@SuppressWarnings("unchecked")
	protected static <T extends Entry<H>, H> Optional<Serie<T, H>> findSerie(List<String> tags) {
		Serie<T, H> serie = null;
		try {
			serie = (Serie<T, H>) SERIES.retrieve(equal(Serie.TAG_ID, Serie.computeTagId(tags))).uniqueResult();
		}
		catch (NoSuchObjectException | NonUniqueObjectException e) {
//			LOG.debug(e);
		}

		return Optional.ofNullable(serie);
	}

	protected static <T extends Entry<H>, H> Serie<T, H> addNewSerie(Metadata metadata) {
		Serie<T, H> serie = new Serie<T, H>(UUID.randomUUID(), metadata);
		SERIES.add(serie);
		SynchronizerRepository.saveSerie(serie);
		return serie;
	}
}
