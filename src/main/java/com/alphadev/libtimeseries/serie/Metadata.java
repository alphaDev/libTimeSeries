package com.alphadev.libtimeseries.serie;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Metadata implements Serializable {
	private static final long	serialVersionUID	= 3177921622298716109L;
	private final List<String>	tags;
	private String				unit;
	private ValueFormat			valueFormat			= ValueFormat.LONG;

	public Metadata(List<String> tags, String unit, ValueFormat valueFormat) {
		this.tags = Collections.unmodifiableList(tags);
		this.unit = unit;
		this.valueFormat = valueFormat;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Metadata)) {
			return false;
		}
		Metadata serieMetadata = (Metadata) obj;
		return Objects.equals(tags, serieMetadata.tags) && Objects.equals(unit, serieMetadata.unit) && Objects.equals(valueFormat, serieMetadata.valueFormat);
	}

	@Override
	public int hashCode() {
		return Objects.hash(tags, unit, valueFormat);
	}

	public List<String> getTags() {
		return tags;
	}

	public ValueFormat getValueFormat() {
		return valueFormat;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
