package com.alphadev.libtimeseries.serie.driver;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.Entry;

public interface Encoder<T extends Entry<H>, H> {

	public void encodeInitialEntry(T entry, Buffer buffer, Cursor cursor);

	default public void encodeRefDiffTime(int refDiffTime, Buffer buffer, Cursor cursor) {
		buffer.appendLong(cursor, refDiffTime, Integer.SIZE);
	}

	public void encodeNextEntry(T lastEntry, T currentEntry, int refDiffTime, Buffer buffer, Cursor cursor);
}
