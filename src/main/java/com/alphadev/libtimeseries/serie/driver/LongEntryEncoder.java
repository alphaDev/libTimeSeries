package com.alphadev.libtimeseries.serie.driver;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.LongEntry;

public final class LongEntryEncoder implements Encoder<LongEntry, Long> {
	private static LongEntryEncoder instance = new LongEntryEncoder();

	private LongEntryEncoder() {}

	public static LongEntryEncoder getInstance() {
		return instance;
	}

	@Override
	public void encodeInitialEntry(LongEntry entry, Buffer buffer, Cursor cursor) {
		buffer.appendLong(cursor, entry.getTimestamp(), Long.SIZE);
		buffer.appendLong(cursor, entry.getValue(), Long.SIZE);
	}

	@Override
	public void encodeNextEntry(LongEntry lastEntry, LongEntry currentEntry, int refDiffTime, Buffer buffer, Cursor cursor) {
		long diffTime = currentEntry.getTimestamp() - lastEntry.getTimestamp() - refDiffTime;
		long xorValue = currentEntry.getValue() ^ lastEntry.getValue();

		boolean noDiffTime = diffTime == 0;
		buffer.appendBit(cursor, noDiffTime);

		if (!noDiffTime) {
			boolean diffTimeIsNegatif = (diffTime & 0x8000000000000000L) == 0x8000000000000000L;

			long diffTimeAbs = diffTime;
			if (diffTimeIsNegatif) {
				diffTimeAbs = (~diffTime) + 1;
			}
			int diffTimeSize = Long.SIZE - Long.numberOfLeadingZeros(diffTimeAbs);

			buffer.appendLong(cursor, diffTimeSize, DriverConst.BIT_SIZE_OF_SIZE);
			buffer.appendBit(cursor, diffTimeIsNegatif);
			buffer.appendLong(cursor, diffTimeAbs, diffTimeSize);
		}

		boolean noDiffValue = xorValue == 0;
		buffer.appendBit(cursor, noDiffValue);
		if (!noDiffValue) {
			int xorValueSize = Long.SIZE - Long.numberOfLeadingZeros(xorValue);
			buffer.appendLong(cursor, xorValueSize, DriverConst.BIT_SIZE_OF_SIZE);
			buffer.appendLong(cursor, xorValue, xorValueSize);
		}
	}
}
