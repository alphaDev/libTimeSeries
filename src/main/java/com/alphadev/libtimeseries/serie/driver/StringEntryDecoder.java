package com.alphadev.libtimeseries.serie.driver;

import java.math.BigInteger;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.StringEntry;

public final class StringEntryDecoder implements Decoder<StringEntry, String> {
	private static StringEntryDecoder instance = new StringEntryDecoder();

	private StringEntryDecoder() {}

	public static StringEntryDecoder getInstance() {
		return instance;
	}

	@Override
	public StringEntry decodeInitialEntry(Buffer buffer, Cursor cursor, StringEntry prevEntry) {
		long timestamp = buffer.readLong(cursor, Long.SIZE);
		int valueSize = (int) buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE_FOR_STRING);
		byte[] valueEncode = buffer.readBytes(cursor, valueSize);
		return new StringEntry(timestamp, valueEncode);
	}

	@Override
	public StringEntry decodeNextEntry(StringEntry prevEntry, int refDiffTime, Buffer buffer, Cursor cursor) {
		long timestamp;
		String value;
		boolean noDiffTime = buffer.readBit(cursor);

		if (noDiffTime) {
			timestamp = prevEntry.getTimestamp() + refDiffTime;
		}
		else {
			int diffTimeSize = (int) buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE);
			boolean bitSign = buffer.readBit(cursor);
			long diffTimeValue = buffer.readLong(cursor, diffTimeSize);
			if (bitSign) {
				diffTimeValue = (~diffTimeValue) + 1;
			}
			timestamp = prevEntry.getTimestamp() + diffTimeValue + refDiffTime;
		}

		boolean noDiffValue = buffer.readBit(cursor);
		if (noDiffValue) {
			value = prevEntry.getValue();
		}
		else {
			int valueSize = (int) buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE_FOR_STRING);
			byte[] xorValue = buffer.readBytes(cursor, valueSize);

			BigInteger pe = new BigInteger(prevEntry.getValueEncode());
			BigInteger xorv = new BigInteger(xorValue);
			BigInteger v = pe.xor(xorv);
			value = StringEntry.decodeValue(v.toByteArray());
		}
		return new StringEntry(timestamp, value);
	}
}
