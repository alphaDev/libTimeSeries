package com.alphadev.libtimeseries.serie.driver;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.Entry;

public interface Decoder<T extends Entry<H>, H> {
	public T decodeInitialEntry(Buffer buffer, Cursor cursor, T prevEntryP);

	default public int decodeRefDiffTime(Buffer buffer, Cursor cursor) {
		return (int) buffer.readLong(cursor, Integer.SIZE);
	}

	public T decodeNextEntry(T prevEntry, int refDiffTime, Buffer buffer, Cursor cursor);
}
