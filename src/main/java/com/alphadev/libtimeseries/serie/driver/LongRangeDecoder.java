package com.alphadev.libtimeseries.serie.driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.LongRange;

public final class LongRangeDecoder implements Decoder<LongRange, Long> {
	private static final Logger	LOG	= LogManager.getLogger(LongRangeDecoder.class);

	private long				margingCreateNewRange;

	public LongRangeDecoder(long margingCreateNewRange) {
		super();
		this.margingCreateNewRange = margingCreateNewRange;
	}

	public LongRange decodeInitialEntry(Buffer buffer, Cursor cursor, LongRange prevEntry) {
		LOG.debug("decodeInitialEntry prevEntry [{}]", prevEntry);
		long start = buffer.readLong(cursor, Long.SIZE);
		long value = buffer.readLong(cursor, Long.SIZE);

		if (prevEntry != null && start - prevEntry.getEnd() <= margingCreateNewRange) {
			prevEntry.setEnd(start);
			if (prevEntry.getValue() == value) {
				return prevEntry;
			}
		}
		return new LongRange(start, start, value);
	}

	public LongRange decodeNextEntry(LongRange prevEntryP, int refDiffTime, Buffer buffer, Cursor cursor) {
		LongRange prevEntry = (LongRange) prevEntryP;
		LongRange nextEntry;
		long timestamp;
		boolean noDiffTime = buffer.readBit(cursor);
		if (noDiffTime) {
			timestamp = prevEntry.getEnd() + refDiffTime;
		}
		else {
			int diffTimeSize = (int) buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE);
			boolean bitSign = buffer.readBit(cursor);
			long diffTimeValue = buffer.readLong(cursor, diffTimeSize);
			if (bitSign) {
				diffTimeValue = (~diffTimeValue) + 1;
			}
			timestamp = prevEntry.getEnd() + diffTimeValue + refDiffTime;
		}
		boolean rangeNotTooOld = timestamp - prevEntry.getEnd() <= margingCreateNewRange;
		if (rangeNotTooOld) {
			prevEntry.setEnd(timestamp);
		}

		boolean noDiffValue = buffer.readBit(cursor);
		if (noDiffValue) {
			if (rangeNotTooOld) {
				nextEntry = prevEntry;
			}
			else {
				nextEntry = new LongRange(timestamp, timestamp, prevEntry.getValue());
			}
		}
		else {
			int xorValueSize = (int) buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE);
			long xorValue = buffer.readLong(cursor, xorValueSize);
			long value = prevEntry.getValue() ^ xorValue;
			nextEntry = new LongRange(timestamp, timestamp, value);
		}
		return nextEntry;
	}
}
