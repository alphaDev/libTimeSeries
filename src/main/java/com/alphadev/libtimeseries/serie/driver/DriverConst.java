package com.alphadev.libtimeseries.serie.driver;

public final class DriverConst {
	public static final int	BIT_SIZE_OF_SIZE			= 6;
	public static final int	BIT_SIZE_OF_SIZE_FOR_STRING	= 8;

	private DriverConst() {}
}
