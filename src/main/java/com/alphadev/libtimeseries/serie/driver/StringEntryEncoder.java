package com.alphadev.libtimeseries.serie.driver;

import java.math.BigInteger;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.StringEntry;

public final class StringEntryEncoder implements Encoder<StringEntry, String> {
	private static StringEntryEncoder instance = new StringEntryEncoder();

	private StringEntryEncoder() {}

	public static StringEntryEncoder getInstance() {
		return instance;
	}

	@Override
	public void encodeInitialEntry(StringEntry entry, Buffer buffer, Cursor cursor) {
		byte[] valueBytes = entry.getValueEncode();

		buffer.appendLong(cursor, entry.getTimestamp(), Long.SIZE);
		buffer.appendLong(cursor, valueBytes.length, DriverConst.BIT_SIZE_OF_SIZE_FOR_STRING);
		buffer.appendBytes(cursor, valueBytes);
	}

	@Override
	public void encodeNextEntry(StringEntry lastEntry, StringEntry currentEntry, int refDiffTime, Buffer buffer, Cursor cursor) {
		long diffTime = currentEntry.getTimestamp() - lastEntry.getTimestamp() - refDiffTime;
		BigInteger ce = new BigInteger(currentEntry.getValueEncode());
		BigInteger le = new BigInteger(lastEntry.getValueEncode());
		BigInteger xorValue = ce.xor(le);

		boolean noDiffTime = diffTime == 0;
		buffer.appendBit(cursor, noDiffTime);

		if (!noDiffTime) {
			boolean diffTimeIsNegatif = (diffTime & 0x8000000000000000L) == 0x8000000000000000L;

			long diffTimeAbs = diffTime;
			if (diffTimeIsNegatif) {
				diffTimeAbs = (~diffTime) + 1;
			}
			int diffTimeSize = Long.SIZE - Long.numberOfLeadingZeros(diffTimeAbs);

			buffer.appendLong(cursor, diffTimeSize, DriverConst.BIT_SIZE_OF_SIZE);
			buffer.appendBit(cursor, diffTimeIsNegatif);
			buffer.appendLong(cursor, diffTimeAbs, diffTimeSize);
		}

		boolean noDiffValue = xorValue.equals(BigInteger.ZERO);
		buffer.appendBit(cursor, noDiffValue);
		if (!noDiffValue) {
			byte[] tmp = xorValue.toByteArray();
			buffer.appendLong(cursor, tmp.length, DriverConst.BIT_SIZE_OF_SIZE_FOR_STRING);
			buffer.appendBytes(cursor, tmp);
		}
	}
}
