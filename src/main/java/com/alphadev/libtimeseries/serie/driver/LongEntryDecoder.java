package com.alphadev.libtimeseries.serie.driver;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.entry.LongEntry;

public final class LongEntryDecoder implements Decoder<LongEntry, Long> {

	private static LongEntryDecoder instance = new LongEntryDecoder();

	private LongEntryDecoder() {}

	public static LongEntryDecoder getInstance() {
		return instance;
	}

	@Override
	public LongEntry decodeInitialEntry(Buffer buffer, Cursor cursor, LongEntry prevEntryP) {
		return new LongEntry(buffer.readLong(cursor, Long.SIZE), buffer.readLong(cursor, Long.SIZE));
	}

	@Override
	public LongEntry decodeNextEntry(LongEntry prevEntry, int refDiffTime, Buffer buffer, Cursor cursor) {
		long timestamp;
		long value;

		boolean noDiffTime = buffer.readBit(cursor);
		if (noDiffTime) {
			timestamp = prevEntry.getTimestamp() + refDiffTime;
		}
		else {
			int diffTimeSize = (int) buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE);
			boolean bitSign = buffer.readBit(cursor);
			long diffTimeValue = buffer.readLong(cursor, diffTimeSize);
			if (bitSign) {
				diffTimeValue = (~diffTimeValue) + 1;
			}
			timestamp = prevEntry.getTimestamp() + diffTimeValue + refDiffTime;
		}

		boolean noDiffValue = buffer.readBit(cursor);
		if (noDiffValue) {
			value = prevEntry.getValue();
		}
		else {
			int xorValueSize = (int) buffer.readLong(cursor, DriverConst.BIT_SIZE_OF_SIZE);
			long xorValue = buffer.readLong(cursor, xorValueSize);
			value = prevEntry.getValue() ^ xorValue;
		}
		return new LongEntry(timestamp, value);
	}

}
