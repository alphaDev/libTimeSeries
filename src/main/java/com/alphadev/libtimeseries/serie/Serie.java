package com.alphadev.libtimeseries.serie;

import static com.googlecode.cqengine.query.QueryFactory.attribute;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import com.alphadev.libtimeseries.serie.driver.Decoder;
import com.alphadev.libtimeseries.serie.driver.Encoder;
import com.alphadev.libtimeseries.serie.driver.LongEntryDecoder;
import com.alphadev.libtimeseries.serie.driver.LongEntryEncoder;
import com.alphadev.libtimeseries.serie.driver.LongRangeDecoder;
import com.alphadev.libtimeseries.serie.driver.StringEntryDecoder;
import com.alphadev.libtimeseries.serie.driver.StringEntryEncoder;
import com.alphadev.libtimeseries.serie.entry.Entry;
import com.alphadev.libtimeseries.serie.entry.LongRange;
import com.googlecode.cqengine.attribute.Attribute;

public class Serie<T extends Entry<H>, H> implements Serializable {
	private static final long														serialVersionUID	= 6779763449682110676L;

	public static final transient Attribute<Serie<? extends Entry<?>, ?>, String>	TAG_ID				= attribute(Serie::getTagId);

	private final UUID																id;
	private final Metadata															metadata;
	private transient SerieWriter<T, H>												writer;
	private transient SerieReader<T, H>												reader;

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Serie other = (Serie) obj;
		if (id == null) {
			if (other.id != null) return false;
		}
		else if (!id.equals(other.id)) return false;
		if (metadata == null) {
			if (other.metadata != null) return false;
		}
		else if (!metadata.equals(other.metadata)) return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((metadata == null) ? 0 : metadata.hashCode());
		return result;
	}

	public static String computeTagId(List<String> tags) {
		return String.join("^~^__^~^", tags);
	}

	public Serie(UUID id, Metadata metadata) {
		this.id = id;
		this.metadata = metadata;
		init();
	}

	public UUID getId() {
		return id;
	}

	public void add(T entry) {
		writer.add(entry);
	}

	public List<T> read(long start, long end) {
		return reader.read(start, end);
	}

	public List<LongRange> readRange(long start, long end, long margingCreateNewRange) {
		SerieReader<LongRange, Long> reader = new SerieReader<LongRange, Long>(id, new LongRangeDecoder(margingCreateNewRange));
		return reader.read(start, end);
	}

	@SuppressWarnings("unchecked")
	public void init() {
		Encoder<T, H> entryEncoder = null;
		Decoder<T, H> entryDecoder = null;
		switch (getValueFormat()) {
			case STRING:
				entryEncoder = (Encoder<T, H>) StringEntryEncoder.getInstance();
				entryDecoder = (Decoder<T, H>) StringEntryDecoder.getInstance();
			break;
			case FIXED_POINT:
			case FLOAT:
			case LONG:
			default:
				entryEncoder = (Encoder<T, H>) LongEntryEncoder.getInstance();
				entryDecoder = (Decoder<T, H>) LongEntryDecoder.getInstance();
			break;
		}

		writer = new SerieWriter<T, H>(id, entryEncoder);
		reader = new SerieReader<T, H>(id, entryDecoder);
	}

	@Override
	public String toString() {
		return String.join(",", getTags());
	}

	private List<String> getTags() {
		return metadata.getTags();
	}

	private ValueFormat getValueFormat() {
		return metadata.getValueFormat();
	}

	public Metadata getMetadata() {
		return metadata;
	}

	public void setMetadata(Metadata metadata) {
		this.metadata.setUnit(metadata.getUnit());
	}

	private String getTagId() {
		return computeTagId(getTags());
	}

}
