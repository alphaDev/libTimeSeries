package com.alphadev.libtimeseries.serie;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferRepository;
import com.alphadev.libtimeseries.buffer.Cursor;
import com.alphadev.libtimeseries.serie.driver.Decoder;
import com.alphadev.libtimeseries.serie.entry.Entry;

public class SerieReader<T extends Entry<H>, H> {
	private UUID			id;
	private Decoder<T, H>	entryDecoder;

	public SerieReader(UUID id, Decoder<T, H> entryDecoder) {
		this.id = id;
		this.entryDecoder = entryDecoder;
	}

	public List<T> read(long start, long end) {
		List<Buffer> buffers = BufferRepository.findBuffers(id, start, end);

		List<T> entries = new ArrayList<>();

		T prevEntry = null;
		for (Buffer buffer : buffers) {
			Cursor cursor = new Cursor();
			T nextEntry = entryDecoder.decodeInitialEntry(buffer, cursor, prevEntry);
			int refDiffTime = entryDecoder.decodeRefDiffTime(buffer, cursor);
			if (nextEntry != prevEntry && start <= nextEntry.getTimestamp() && nextEntry.getTimestamp() <= end) {
				entries.add(nextEntry);
			}
			prevEntry = nextEntry;

			while (buffer.getBitSize() > cursor.getBitCursor()) {
				nextEntry = entryDecoder.decodeNextEntry(prevEntry, refDiffTime, buffer, cursor);
				// Yes i intentionally not use equals for nextEntry != prevEntry
				if (nextEntry != prevEntry && start <= nextEntry.getTimestamp() && nextEntry.getTimestamp() <= end) {
					entries.add(nextEntry);
				}
				prevEntry = nextEntry;
			}
		}

		return entries;
	}

}
