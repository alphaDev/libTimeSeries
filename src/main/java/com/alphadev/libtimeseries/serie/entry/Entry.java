package com.alphadev.libtimeseries.serie.entry;

public abstract class Entry<T> {
	protected long	timestamp	= -1;
	protected T		value;

	public long getTimestamp() {
		return timestamp;
	}

	public T getValue() {
		return value;
	}

}
