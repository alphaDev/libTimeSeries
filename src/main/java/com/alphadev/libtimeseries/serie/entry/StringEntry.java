package com.alphadev.libtimeseries.serie.entry;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.alphadev.libtimeseries.TimeSeriesException;

public class StringEntry extends Entry<String> {
	private byte[] valueEncode;

	public StringEntry() {}

	public StringEntry(long timestamp, String value) {
		this.timestamp = timestamp;
		this.value = value;
	}

	public StringEntry(long timestamp, byte[] valueEncode) {
		this.timestamp = timestamp;
		this.value = decodeValue(valueEncode);
	}

	public static String decodeValue(byte[] valueEncode) {
		try {
			return new String(valueEncode, StandardCharsets.UTF_8.name());
		}
		catch (UnsupportedEncodingException e) {
			throw new TimeSeriesException("Fail to encode next valueEncode [" + valueEncode + "]");
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());

		sb.append("[");
		sb.append(formatter.format(Instant.ofEpochSecond(timestamp)));
		sb.append(" : ");
		sb.append(value);
		sb.append("]");

		return sb.toString();
	}

	public byte[] getValueEncode() {
		if (valueEncode == null) {
			try {
				valueEncode = value.getBytes(StandardCharsets.UTF_8.name());
			}
			catch (UnsupportedEncodingException e) {
				throw new TimeSeriesException("Fail to encode next entry [" + this + "]");
			}
		}
		return valueEncode;
	}

	public void setEntry(long timestamp, String value) {
		this.timestamp = timestamp;
		this.value = value;
	}

	public void setEntry(StringEntry entry) {
		this.timestamp = entry.timestamp;
		this.value = entry.value;

	}

	@Override
	public boolean equals(Object o) {

		if (o == this) {
			return true;
		}
		if (!(o instanceof StringEntry)) {
			return false;
		}
		StringEntry entry = (StringEntry) o;
		return timestamp == entry.timestamp && StringUtils.equals(entry.value, value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(timestamp, value);
	}
}
