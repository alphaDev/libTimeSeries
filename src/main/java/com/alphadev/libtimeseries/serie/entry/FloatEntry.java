package com.alphadev.libtimeseries.serie.entry;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

public class FloatEntry extends Entry<Float> {
	private long timestamp = -1;

	public FloatEntry() {}

	public FloatEntry(long timestamp, Float value) {
		this.timestamp = timestamp;
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());

		sb.append("[");
		sb.append(formatter.format(Instant.ofEpochSecond(timestamp)));
		sb.append(" : ");
		sb.append(value);
		sb.append("]");

		return sb.toString();
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setEntry(long timestamp, Float value) {
		this.timestamp = timestamp;
		this.value = value;
	}

	public void setEntry(FloatEntry entry) {
		this.timestamp = entry.timestamp;
		this.value = entry.value;

	}

	@Override
	public boolean equals(Object o) {

		if (o == this) {
			return true;
		}
		if (!(o instanceof FloatEntry)) {
			return false;
		}
		FloatEntry entry = (FloatEntry) o;
		return timestamp == entry.timestamp && entry.value == value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(timestamp, value);
	}
}
