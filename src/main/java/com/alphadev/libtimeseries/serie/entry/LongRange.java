package com.alphadev.libtimeseries.serie.entry;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class LongRange extends Entry<Long> {
	private long	start;
	private long	end;

	public LongRange() {
	}

	public LongRange(long start, long end, Long value) {
		this.start = start;
		this.end = end;
		this.value = value;
		this.timestamp = start;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());

		sb.append("[");
		sb.append(formatter.format(Instant.ofEpochSecond(start)));
		sb.append("-");
		sb.append(formatter.format(Instant.ofEpochSecond(end)));
		sb.append(" : ");
		sb.append(value);
		sb.append("]");

		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (end ^ (end >>> 32));
		result = prime * result + (int) (start ^ (start >>> 32));
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		LongRange other = (LongRange) obj;
		if (end != other.end) return false;
		if (start != other.start) return false;
		if (value == null) {
			if (other.value != null) return false;
		}
		else if (!value.equals(other.value)) return false;
		return true;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public Long getValue() {
		return value;
	}

	public long getStart() {
		return start;
	}

}
