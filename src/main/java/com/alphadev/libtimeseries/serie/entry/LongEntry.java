package com.alphadev.libtimeseries.serie.entry;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;

public class LongEntry extends Entry<Long> {
	public LongEntry() {}

	public LongEntry(long timestamp, long value) {
		this.timestamp = timestamp;
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());

		sb.append("[");
		sb.append(formatter.format(Instant.ofEpochSecond(timestamp)));
		sb.append(" : ");
		sb.append(value);
		sb.append("]");

		return sb.toString();
	}

//	public long getValue() {
//		return value;
//	}

	public void setEntry(long timestamp, long value) {
		this.timestamp = timestamp;
		this.value = value;
	}

	public void setEntry(LongEntry entry) {
		this.timestamp = entry.timestamp;
		this.value = entry.value;

	}

	@Override
	public boolean equals(Object o) {

		if (o == this) {
			return true;
		}
		if (!(o instanceof LongEntry)) {
			return false;
		}
		LongEntry entry = (LongEntry) o;
		return timestamp == entry.timestamp && entry.value == value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(timestamp, value);
	}
}
