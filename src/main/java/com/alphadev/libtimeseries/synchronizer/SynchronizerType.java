package com.alphadev.libtimeseries.synchronizer;

public enum SynchronizerType {
	ROCKSDB, MEMORY;
}
