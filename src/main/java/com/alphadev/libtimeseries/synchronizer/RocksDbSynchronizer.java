package com.alphadev.libtimeseries.synchronizer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rocksdb.ColumnFamilyDescriptor;
import org.rocksdb.ColumnFamilyHandle;
import org.rocksdb.ColumnFamilyOptions;
import org.rocksdb.DBOptions;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksDBException;
import org.rocksdb.RocksIterator;

import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.buffer.BufferRepository;
import com.alphadev.libtimeseries.serie.Serie;
import com.alphadev.libtimeseries.serie.ToWriteBuffer;
import com.alphadev.libtimeseries.serie.entry.Entry;

public class RocksDbSynchronizer implements Synchronizer {
	private static final Logger				LOG						= LogManager.getLogger(RocksDbSynchronizer.class);

	private String							name;
	private RocksDB							db;
	private final List<ColumnFamilyHandle>	columnFamilyHandleList	= new ArrayList<>();
	private ColumnFamilyHandle				knownSeries;
	private ColumnFamilyHandle				buffers;

	public RocksDbSynchronizer(String name) {
		this.name = name;

	}

	@Override
	public void load() {
		LOG.debug("loading RocksDbSynchronizer for db [{}]", name);

		if (db == null) {
			RocksDB.loadLibrary();
			final ColumnFamilyOptions columnFamilyOptionsCreator = new ColumnFamilyOptions();
			final ColumnFamilyOptions columnFamilyOptions = columnFamilyOptionsCreator.optimizeUniversalStyleCompaction();

			final List<ColumnFamilyDescriptor> cfDescriptors = Arrays.asList( //
			        new ColumnFamilyDescriptor(RocksDB.DEFAULT_COLUMN_FAMILY, columnFamilyOptions), //
			        new ColumnFamilyDescriptor("known-series".getBytes(), columnFamilyOptions), //
			        new ColumnFamilyDescriptor("buffers".getBytes(), columnFamilyOptions));

			final DBOptions optionsCreator = new DBOptions();
			final DBOptions options = optionsCreator.setCreateIfMissing(true).setCreateMissingColumnFamilies(true);

			try {
				db = RocksDB.open(options, name + "-lib-ts", cfDescriptors, columnFamilyHandleList);
			}
			catch (RocksDBException e) {
				LOG.error("load error.", e);
				throw new TimeSeriesException(e);
			}
			finally {
				options.close();
				optionsCreator.close();
				columnFamilyOptions.close();
				columnFamilyOptionsCreator.close();
			}

			knownSeries = columnFamilyHandleList.get(1);
			buffers = columnFamilyHandleList.get(2);
		}

	}

	@Override
	public Collection<Serie<? extends Entry<?>, ?>> getKnownSeries() {
//		LOG.debug("getKnownSeries");
		Collection<Serie<? extends Entry<?>, ?>> series = new ArrayList<>();
		try (final RocksIterator iterator = db.newIterator(knownSeries)) {
			for (iterator.seekToFirst(); iterator.isValid(); iterator.next()) {
				byte[] value = iterator.value();

				ByteArrayInputStream bStream = new ByteArrayInputStream(value);
				ObjectInputStream ois = new ObjectInputStream(bStream);
				Serie<?, ?> readSerie = (Serie<?, ?>) ois.readObject();
				series.add(readSerie);
//				LOG.debug("readSerie [{}]", readSerie);
				ois.close();
				bStream.close();
			}
		}
		catch (IOException | ClassNotFoundException e) {
			LOG.error("getKnownSeries error.", e);
			throw new TimeSeriesException(e);
		}
		return series;
	}

	@Override
	public void saveSerie(Serie<? extends Entry<?>, ?> serie) {
		// LOG.debug("addSerie [{}]", serie);
		ByteArrayOutputStream bStream = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bStream);
			oos.writeObject(serie);
			oos.flush();
			bStream.flush();
			db.put(knownSeries, serie.getId().toString().getBytes(), bStream.toByteArray());

			LOG.debug("addSerie [{}]", serie);
			oos.close();
			bStream.close();
		}
		catch (IOException | RocksDBException e) {
			LOG.error("addSerie error.", e);
			throw new TimeSeriesException(e);
		}
	}

	@Override
	public void deleteSerie(Serie<? extends Entry<?>, ?> serie) {
		try {
			db.delete(knownSeries, serie.getId().toString().getBytes());
		}
		catch (RocksDBException e) {
			LOG.error("deleteSerie error.", e);
			throw new TimeSeriesException(e);
		}

	}

	@Override
	public Collection<BufferId> getKnownBuffers() {
		Collection<BufferId> knownBuffers = new ArrayList<>();
		try (final RocksIterator iterator = db.newIterator(buffers)) {
			for (iterator.seekToFirst(); iterator.isValid(); iterator.next()) {
				byte[] key = iterator.key();
				knownBuffers.add(BufferId.unserialize(key));
			}
		}
		return knownBuffers;
	}

	@Override
	public Buffer loadBuffer(BufferId bufferId) {
		ByteBuffer buf = null;
		try {
			buf = ByteBuffer.wrap(db.get(buffers, bufferId.serialize()));

			int sizeBits = buf.getInt();
			int longArraySize = buf.remaining() / Long.BYTES + 1;
			long[] longBuffer = new long[longArraySize];
			int i = 0;
			while (buf.hasRemaining()) {
				longBuffer[i++] = buf.getLong();
			}

			return BufferRepository.makeBuffer(bufferId, longBuffer, sizeBits);
		}
		catch (RocksDBException e) {
			LOG.error("loadBuffer error.", e);
			throw new TimeSeriesException(e);
		}
	}

	@Override
	public void writeBuffer(ToWriteBuffer buffer) {
		try {
			db.put(buffers, buffer.getBuffeId().serialize(), buffer.getBuffer().array());
//			LOG.debug("writeBuffer [{}]", buffer);
		}
		catch (RocksDBException e) {
			LOG.error("writeBuffer error.", e);
			throw new TimeSeriesException(e);
		}
	}

	@Override
	public void deleteBuffers(List<BufferId> buffers) {
		for (BufferId bufferId : buffers) {
			try {
				db.delete(this.buffers, bufferId.serialize());
			}
			catch (RocksDBException e) {
				LOG.error("writeBuffer error.", e);
				throw new TimeSeriesException(e);
			}
		}
	}

	@Override
	public void close() {
		for (final ColumnFamilyHandle columnFamilyHandle : columnFamilyHandleList) {
			columnFamilyHandle.close();
		}
		db.close();
		knownSeries = null;
		buffers = null;
		db = null;
		LOG.debug("close");
	}

}
