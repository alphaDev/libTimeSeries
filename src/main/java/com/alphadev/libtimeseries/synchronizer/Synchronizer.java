package com.alphadev.libtimeseries.synchronizer;

import java.util.Collection;
import java.util.List;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.serie.Serie;
import com.alphadev.libtimeseries.serie.ToWriteBuffer;
import com.alphadev.libtimeseries.serie.entry.Entry;

public interface Synchronizer {

	public void load();

	public Collection<Serie<? extends Entry<?>, ?>> getKnownSeries();

	public Collection<BufferId> getKnownBuffers();

	public Buffer loadBuffer(BufferId bufferId);

	public void writeBuffer(ToWriteBuffer buffer);

	public void close();

	public void saveSerie(Serie<? extends Entry<?>, ?> serie);

	public void deleteSerie(Serie<? extends Entry<?>, ?> serie);

	public void deleteBuffers(List<BufferId> buffers);
}
