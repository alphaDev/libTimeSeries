package com.alphadev.libtimeseries.synchronizer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.serie.Serie;
import com.alphadev.libtimeseries.serie.ToWriteBuffer;
import com.alphadev.libtimeseries.serie.entry.Entry;

public class SynchronizerRepository {
	private static Synchronizer synchronizer;

	private SynchronizerRepository() {}

	public static void load(String dbname, SynchronizerType synchronizerName) {
		Map<SynchronizerType, Class<? extends Synchronizer>> synchronizers = new EnumMap<>(SynchronizerType.class);
		synchronizers.put(SynchronizerType.ROCKSDB, RocksDbSynchronizer.class);
		synchronizers.put(SynchronizerType.MEMORY, MemorySynchronizer.class);

		Class<? extends Synchronizer> classLoader = synchronizers.get(synchronizerName);

		try {
			Constructor<? extends Synchronizer> constructor = classLoader.getConstructor(String.class);
			synchronizer = constructor.newInstance(dbname);
			synchronizer.load();
		}
		catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new TimeSeriesException("Cannot create the synchronizer [" + synchronizerName + "] for db [" + dbname + "]", e);
		}
	}

	public static void close() {
		synchronizer.close();
	}

	public static Collection<Serie<? extends Entry<?>, ?>> getKnownSeries() {
		Collection<Serie<? extends Entry<?>, ?>> knownSeries = synchronizer.getKnownSeries();
		knownSeries.forEach(Serie::init);
		return knownSeries;
	}

	public static Collection<BufferId> getKnownBuffers() {
		return synchronizer.getKnownBuffers();
	}

	public static void saveSerie(Serie<? extends Entry<?>, ?> serie) {
		synchronizer.saveSerie(serie);
	}

	public static Buffer loadBuffer(BufferId bufferId) {
		return synchronizer.loadBuffer(bufferId);
	}

	public static void writeBuffer(ToWriteBuffer buffer) {
		synchronizer.writeBuffer(buffer);
	}

	public static void deleteSerie(Serie<? extends Entry<?>, ?> serie) {
		synchronizer.deleteSerie(serie);
	}

	public static void deleteBuffers(List<BufferId> buffers) {
		synchronizer.deleteBuffers(buffers);

	}

}
