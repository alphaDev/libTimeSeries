package com.alphadev.libtimeseries.synchronizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.libtimeseries.buffer.Buffer;
import com.alphadev.libtimeseries.buffer.BufferId;
import com.alphadev.libtimeseries.buffer.BufferRepository;
import com.alphadev.libtimeseries.serie.Serie;
import com.alphadev.libtimeseries.serie.ToWriteBuffer;
import com.alphadev.libtimeseries.serie.entry.Entry;

public class MemorySynchronizer implements Synchronizer {
	private static final Logger							LOG	= LogManager.getLogger(MemorySynchronizer.class);

	private String										name;
	private Collection<Serie<? extends Entry<?>, ?>>	knownSeries;
	private Map<BufferId, ToWriteBuffer>				buffers;

	public MemorySynchronizer(String name) {
		this.name = name;
		knownSeries = new ArrayList<>();
		buffers = new HashMap<>();
	}

	@Override
	public void load() {
		LOG.debug("loading MemorySynchronizer for db [{}]", name);
	}

	@Override
	public Collection<Serie<? extends Entry<?>, ?>> getKnownSeries() {
		return knownSeries;
	}

	@Override
	public Collection<BufferId> getKnownBuffers() {
		return buffers.keySet();
	}

	@Override
	public Buffer loadBuffer(BufferId bufferId) {
		ToWriteBuffer buffer = buffers.get(bufferId);
		long[] longBuffer = ArrayUtils.toPrimitive(Stream.iterate(0, i -> i + 1).limit(buffer.getBuffer().asLongBuffer().limit()).map(i -> buffer.getBuffer().asLongBuffer().get(i)).collect(Collectors.toList()).toArray(new Long[0]));
		return BufferRepository.makeBuffer(bufferId, longBuffer, buffer.getSizeBits());
	}

	@Override
	public void writeBuffer(ToWriteBuffer buffer) {
		buffers.put(buffer.getBuffeId(), buffer);
	}

	@Override
	public void saveSerie(Serie<? extends Entry<?>, ?> serie) {
		if (!knownSeries.contains(serie)) {
			knownSeries.add(serie);
		}
	}

	@Override
	public void deleteSerie(Serie<? extends Entry<?>, ?> serie) {
		knownSeries.remove(serie);
	}

	@Override
	public void deleteBuffers(List<BufferId> buffers) {
		for (BufferId bufferId : buffers) {
			this.buffers.remove(bufferId);
		}
	}

	@Override
	public void close() {
		knownSeries.clear();
		buffers.clear();
	}

}
