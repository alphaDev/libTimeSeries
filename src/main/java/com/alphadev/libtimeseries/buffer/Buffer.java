package com.alphadev.libtimeseries.buffer;

import java.nio.BufferOverflowException;
import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.libtimeseries.TimeSeriesException;

public final class Buffer {
	private static final Logger				LOG					= LogManager.getLogger(Buffer.class);
	private static final long[]				MASKS				= initMasks();
	private static final int				LONG_BIT_SIZE		= Long.SIZE;
	private static final int				INITIAL_SIZE		= 2048;

	public static final Comparator<Buffer>	COMPARATOR_BY_TIME	= ((buffer1, buffer2) -> Long.compare(buffer1.getBufferId().getRefTime(), buffer2.getBufferId().getRefTime()));

	private long[]							bufferArray;
	private int								bitSize;

	private final BufferId					bufferId;

	protected Buffer(BufferId bufferId) {
		this.bufferId = bufferId;
		bufferArray = new long[INITIAL_SIZE];
		bitSize = 0;
	}

	protected Buffer(BufferId bufferId, long[] bufferArray, int bitSize) {
		this.bufferId = bufferId;
		this.bufferArray = bufferArray;
		this.bitSize = bitSize;
	}

	private static long[] initMasks() {
		long[] masks = new long[LONG_BIT_SIZE];
		for (int i = 0; i < LONG_BIT_SIZE; i++) {
			masks[i] = (i == 0 ? 0 : masks[i - 1]) + (1l << i);
		}
		return masks;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (long b : bufferArray) {
			sb.append(StringUtils.leftPad(Long.toBinaryString(b), LONG_BIT_SIZE, '0')).append("|");
		}

		return sb.toString();
	}

	public boolean readBit(Cursor cursor) {
		if (cursor.getBitCursor() + 1 > bitSize) {
			throw new BufferOverflowException();
		}

		long mask = 1l << cursor.getBitInLongCursor();
		boolean value = (bufferArray[cursor.getLongCursor()] & mask) == mask;

		cursor.nextBit();
		return value;
	}

	public long readLong(Cursor cursor, int valueSize) {
		if (!(0 < valueSize && valueSize <= Long.SIZE)) {
			throw new TimeSeriesException("Invalide bits size:" + valueSize);
		}
		if (cursor.getBitCursor() + valueSize > bitSize) {
			throw new TimeSeriesException("cannot read bit [" + valueSize + "] from [" + cursor.getBitCursor() + "] buffer size [" + bitSize + "]");
		}

		long value = 0;
		int bitInLongCursor = cursor.getBitInLongCursor();
		int longCursor = cursor.getLongCursor();
		int offset = bitInLongCursor - valueSize + 1;

		if (0 <= offset) {
			long mask = MASKS[valueSize - 1] << offset;
			value = (bufferArray[longCursor] & mask) >>> (bitInLongCursor + 1 - valueSize);
		}
		else {
			int part01Size = bitInLongCursor + 1;
			int part02Size = valueSize - part01Size;
			value = (bufferArray[longCursor] & MASKS[bitInLongCursor]) << part02Size;
			bitInLongCursor = LONG_BIT_SIZE - part02Size;
			long mask2 = MASKS[part02Size - 1] << (bitInLongCursor);
			value += (bufferArray[++longCursor] & mask2) >>> (bitInLongCursor);
		}

		cursor.nextBits(valueSize);
		return value;
	}

	public byte readByte(Cursor cursor) {
		return (byte) (0xffl & readLong(cursor, Byte.SIZE));
	}

	public byte[] readBytes(Cursor cursor, int size) {
		byte[] bytes = new byte[size];
		for (int i = 0; i < size; i++) {
			bytes[i] = readByte(cursor);
		}
		return bytes;
	}

	public void appendBit(Cursor cursor, boolean value) {
		if (value) {
			long mask = 1l << cursor.getBitInLongCursor();
			bufferArray[cursor.getLongCursor()] |= mask;
		}

		cursor.nextBit();
		bitSize++;
		if (cursor.onNextLong()) {
			growBufferSize(cursor.getLongCursor());
		}
	}

	public void appendLong(Cursor cursor, long value, int valueSize) {
		if (!(0 < valueSize && valueSize <= Long.SIZE)) {
			throw new TimeSeriesException("Invalide bits size:" + valueSize);
		}

		int bitInLongCursor = cursor.getBitInLongCursor();
		int longCursor = cursor.getLongCursor();
		int offset = bitInLongCursor - valueSize + 1;
		bitSize += valueSize;
		if (0 <= offset) {
			long mask = value << offset;
			bufferArray[longCursor] |= mask;

			cursor.nextBits(valueSize);
			if (cursor.onNextLong()) {
				growBufferSize(cursor.getLongCursor());
			}
		}
		else {
			int part01Size = valueSize - bitInLongCursor - 1;
			long mask = value >> part01Size;
			bufferArray[longCursor] |= mask;
			bitInLongCursor = LONG_BIT_SIZE - 1;
			longCursor++;
			growBufferSize(longCursor);

			bitInLongCursor -= part01Size;
			mask = value << (bitInLongCursor + 1);
			bufferArray[longCursor] |= mask;

			cursor.nextBits(valueSize);
		}
	}

	public void appendBytes(Cursor cursor, byte[] bytes) {
		for (int i = 0; i < bytes.length; i++) {
			appendByte(cursor, bytes[i]);
		}
	}

	public void appendByte(Cursor cursor, byte value) {
		appendLong(cursor, 0xFFl & value, Byte.SIZE);
	}

	private void growBufferSize(int longCursor) {
		if (longCursor >= bufferArray.length) {
			int newBufferSize = bufferArray.length * 2;
			long[] newBuffer = new long[newBufferSize];
			System.arraycopy(bufferArray, 0, newBuffer, 0, bufferArray.length);
			bufferArray = newBuffer;
			LOG.debug("expend buffer:[{}] bufferWriterArrayCursor:[{}]", newBufferSize, longCursor);
		}
	}

	public long[] getBufferArray() {
		return bufferArray;
	}

	public int getBitSize() {
		return bitSize;
	}

	public BufferId getBufferId() {
		return bufferId;
	}

}
