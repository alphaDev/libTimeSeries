package com.alphadev.libtimeseries.buffer;

import java.nio.ByteBuffer;
import java.util.UUID;

public final class BufferId {
	private final UUID	serieId;
	private final long	refTime;

	public BufferId(UUID serieId, long refTime) {
		this.serieId = serieId;
		this.refTime = refTime;
	}

	public UUID getSerieId() {
		return serieId;
	}

	public long getRefTime() {
		return refTime;
	}

	@Override
	public String toString() {
		return "BufferId[serieId:" + serieId + ", refTime:" + refTime + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof BufferId)) {
			return false;
		}
		BufferId bId = (BufferId) obj;
		return bId == this || (serieId.equals(bId.serieId) && refTime == bId.refTime);
	}

	@Override
	public int hashCode() {
		return (int) (serieId.hashCode() ^ (refTime ^ (refTime >>> 32)));
	}

	public byte[] serialize() {
		ByteBuffer buf = ByteBuffer.allocate(Long.BYTES * 3);

		buf.putLong(serieId.getMostSignificantBits());
		buf.putLong(serieId.getLeastSignificantBits());
		buf.putLong(refTime);

		return buf.array();
	}

	public static BufferId unserialize(byte[] key) {
		ByteBuffer buf = ByteBuffer.wrap(key);
		UUID serieId = new UUID(buf.getLong(), buf.getLong());
		long refTime = buf.getLong();
		return new BufferId(serieId, refTime);
	}

}
