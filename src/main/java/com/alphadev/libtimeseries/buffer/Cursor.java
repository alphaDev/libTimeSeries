package com.alphadev.libtimeseries.buffer;

public final class Cursor {
	public static final int	CURSOR_START_POS	= Long.SIZE - 1;

	private int				bitCursor;
	private int				bitInLongCursor;
	private int				longCursor;

	public Cursor() {
		bitCursor = 0;
		bitInLongCursor = CURSOR_START_POS;
		longCursor = 0;
	}

	protected Cursor(int bitCursor, int bitInLongCursor, int longCursor) {
		this.bitCursor = bitCursor;
		this.bitInLongCursor = bitInLongCursor;
		this.longCursor = longCursor;
	}

	public int getBitCursor() {
		return bitCursor;
	}

	public int getBitInLongCursor() {
		return bitInLongCursor;
	}

	public int getLongCursor() {
		return longCursor;
	}

	public void nextBit() {
		bitCursor++;
		bitInLongCursor--;
		if (bitInLongCursor == -1) {
			bitInLongCursor = CURSOR_START_POS;
			longCursor++;
		}
	}

	public void nextBits(int valueSize) {
		bitCursor += valueSize;
		int offset = bitInLongCursor - valueSize + 1;
		if (0 <= offset) {
			bitInLongCursor -= valueSize;
			if (bitInLongCursor < 0) {
				bitInLongCursor = CURSOR_START_POS;
				longCursor++;
			}
		}
		else {
			bitInLongCursor = Long.SIZE - valueSize + bitInLongCursor;
			longCursor++;
		}
	}

	public boolean onNextLong() {
		return getBitInLongCursor() == CURSOR_START_POS;
	}

}
