package com.alphadev.libtimeseries.buffer;

import static com.googlecode.cqengine.query.QueryFactory.and;
import static com.googlecode.cqengine.query.QueryFactory.between;
import static com.googlecode.cqengine.query.QueryFactory.equal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.libtimeseries.TimeSeriesException;
import com.alphadev.libtimeseries.serie.SerieWriter;
import com.alphadev.libtimeseries.synchronizer.SynchronizerRepository;
import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.index.navigable.NavigableIndex;
import com.googlecode.cqengine.index.unique.UniqueIndex;
import com.googlecode.cqengine.resultset.ResultSet;

public final class BufferRepository {
	private static final Logger									LOG					= LogManager.getLogger(BufferRepository.class);

	private static final IndexedCollection<InternalBufferId>	ALL_KNOW_BUFFERS	= new ConcurrentIndexedCollection<>();
	private static final Map<BufferId, Buffer>					LOAD_BUFFERS		= new HashMap<>();

	private BufferRepository() {}

	public static void clearBuffers() {
		LOAD_BUFFERS.clear();
		ALL_KNOW_BUFFERS.clear();
	}

	public static void load() {
		SynchronizerRepository.getKnownBuffers().parallelStream().forEach(b -> ALL_KNOW_BUFFERS.add(new InternalBufferId(b, false)));
		try {
			ALL_KNOW_BUFFERS.addIndex(UniqueIndex.onAttribute(InternalBufferId.BUFFER_ID));
			ALL_KNOW_BUFFERS.addIndex(HashIndex.onAttribute(InternalBufferId.SERIE_ID));
			ALL_KNOW_BUFFERS.addIndex(NavigableIndex.onAttribute(InternalBufferId.REF_TIME));
		}
		catch (IllegalStateException e) {
			LOG.debug("Skip index creation. They were already made");
		}
	}

	public static Buffer makeNewBuffer(BufferId bufferId) {
		if (LOAD_BUFFERS.containsKey(bufferId)) {
			throw new TimeSeriesException("The buffer [" + bufferId + "] cannot be created, it already exist.");
		}
		Buffer buffer = new Buffer(bufferId);
		LOAD_BUFFERS.put(bufferId, buffer);
		ALL_KNOW_BUFFERS.add(new InternalBufferId(bufferId, true));
		return buffer;
	}

	public static Buffer makeBuffer(BufferId bufferId, long[] bufferArray, int bitSize) {
		return new Buffer(bufferId, bufferArray, bitSize);
	}

	public static Optional<Buffer> getLoadBuffer(BufferId bufferId) {
		return Optional.ofNullable(LOAD_BUFFERS.getOrDefault(bufferId, null));
	}

	public static List<Buffer> findBuffers(UUID serieId, long start, long end) {
		ResultSet<InternalBufferId> results = ALL_KNOW_BUFFERS.retrieve(and(equal(InternalBufferId.SERIE_ID, serieId), between(InternalBufferId.REF_TIME, start - SerieWriter.MAX_DURATION, true, end, true)));

		List<Buffer> buffers = new ArrayList<>();
		for (InternalBufferId bufferResult : results) {
			buffers.add(getBuffers(bufferResult));
		}
		buffers.sort(Buffer.COMPARATOR_BY_TIME);
		return buffers;
	}

	public static void deleteBuffers(UUID serieId) {
		ResultSet<InternalBufferId> results = ALL_KNOW_BUFFERS.retrieve(equal(InternalBufferId.SERIE_ID, serieId));

		List<BufferId> buffers = new ArrayList<>();
		for (InternalBufferId bufferResult : results) {
			buffers.add(bufferResult.getBufferId());
			ALL_KNOW_BUFFERS.remove(bufferResult);
		}
		SynchronizerRepository.deleteBuffers(buffers);
	}

	private static Buffer getBuffers(InternalBufferId bufferResult) {
		BufferId bufferId = bufferResult.getBufferId();
		if (!bufferResult.isLoaded()) {
			Buffer buffer = SynchronizerRepository.loadBuffer(bufferId);
			InternalBufferId internalBufferId = ALL_KNOW_BUFFERS.retrieve(equal(InternalBufferId.BUFFER_ID, bufferId)).uniqueResult();
			internalBufferId.setLoaded(true);
			LOAD_BUFFERS.put(bufferId, buffer);
		}
		return LOAD_BUFFERS.get(bufferId);
	}

}
