package com.alphadev.libtimeseries.buffer;

import static com.googlecode.cqengine.query.QueryFactory.attribute;

import java.util.UUID;

import com.googlecode.cqengine.attribute.Attribute;

class InternalBufferId {
	// -------------------------- Attributes --------------------------
	protected static final Attribute<InternalBufferId, BufferId>	BUFFER_ID	= attribute(InternalBufferId::getBufferId);
	protected static final Attribute<InternalBufferId, Long>		REF_TIME	= attribute(InternalBufferId::getRefTime);
	protected static final Attribute<InternalBufferId, UUID>		SERIE_ID	= attribute(InternalBufferId::getSerieId);

	private final BufferId											bufferId;
	private boolean													loaded;

	protected InternalBufferId(BufferId bufferId, boolean loaded) {
		this.bufferId = bufferId;
		this.loaded = loaded;
	}

	protected BufferId getBufferId() {
		return bufferId;
	}

	protected long getRefTime() {
		return bufferId.getRefTime();
	}

	protected UUID getSerieId() {
		return bufferId.getSerieId();
	}

	protected boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

}
