package com.alphadev.libtimeseries;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alphadev.libtimeseries.buffer.BufferRepository;
import com.alphadev.libtimeseries.serie.Metadata;
import com.alphadev.libtimeseries.serie.SerieRepository;
import com.alphadev.libtimeseries.serie.SerieWriter;
import com.alphadev.libtimeseries.serie.ValueFormat;
import com.alphadev.libtimeseries.serie.entry.Entry;
import com.alphadev.libtimeseries.serie.entry.LongEntry;
import com.alphadev.libtimeseries.serie.entry.LongRange;
import com.alphadev.libtimeseries.serie.entry.StringEntry;
import com.alphadev.libtimeseries.synchronizer.SynchronizerRepository;
import com.alphadev.libtimeseries.synchronizer.SynchronizerType;

public class LibTimeSeries {
	private static final Logger LOG = LogManager.getLogger(LibTimeSeries.class);

	private LibTimeSeries() {}

	public static void load(String dbName, SynchronizerType synchronizerType) {
		SynchronizerRepository.load(dbName, synchronizerType);
		SerieRepository.load();
		BufferRepository.load();
	}

	public static void close() {
		LOG.debug("Closing DB");
		SynchronizerRepository.close();
		SerieRepository.close();
		BufferRepository.clearBuffers();
	}

	public static void createOrUpdateSerie(ValueFormat valueFormat, String unit, String... tags) {
		SerieRepository.createOrUpdateSerie(new Metadata(Arrays.asList(tags), unit, valueFormat));
	}

	public static void add(long time, long value, String... tags) {
		SerieRepository.add(Arrays.asList(tags), new LongEntry(time, value));
	}

	public static void add(long time, String value, String... tags) {
		SerieRepository.add(Arrays.asList(tags), new StringEntry(time, value));
	}

	public static <T extends Entry<H>, H> List<T> read(long start, long end, String... tags) {
		return SerieRepository.read(Arrays.asList(tags), start, end);
	}

	public static List<LongRange> readRange(long start, long end, long marginCreateNewRange, String... tags) {
		return SerieRepository.readRange(Arrays.asList(tags), start, end, marginCreateNewRange);
	}

	public static Set<List<String>> listExisingSerie() {
		return SerieRepository.listExisingSerie();
	}

	public static void delete(String... tags) {
		SerieRepository.delete(Arrays.asList(tags));
	}

	public static void main(String[] args) {
		LibTimeSeries.load("test", SynchronizerType.ROCKSDB);
		delete("serie1");
		delete("serie2");
		createOrUpdateSerie(ValueFormat.LONG, "", "serie1");
		createOrUpdateSerie(ValueFormat.STRING, "", "serie2");
		List<String> serie1Tags = Arrays.asList("serie1");
		List<String> serie2Tags = Arrays.asList("serie2");

		SerieWriter.setMaxDuration(4);
		long time = 1523963343l;
		LOG.debug("save start time [{}]", time);
		SerieRepository.add(serie1Tags, new LongEntry(time, 1l));
		SerieRepository.add(serie1Tags, new LongEntry(time + 1, 2l));
		SerieRepository.add(serie1Tags, new LongEntry(time + 2, 1l));
		SerieRepository.add(serie1Tags, new LongEntry(time + 3, 3l));
		SerieRepository.add(serie1Tags, new LongEntry(time + 4, 4l));

		SerieRepository.add(serie2Tags, new StringEntry(time, "a"));
		SerieRepository.add(serie2Tags, new StringEntry(time + 1, "b"));
		SerieRepository.add(serie2Tags, new StringEntry(time + 2, "c"));
		SerieRepository.add(serie2Tags, new StringEntry(time + 3, "d"));
		SerieRepository.add(serie2Tags, new StringEntry(time + 4, "e"));

		List<LongEntry> valuesSerie1 = SerieRepository.read(serie1Tags, time, time + 5l);
		LOG.debug("values read [{}]", valuesSerie1);
		List<StringEntry> valuesSerie2 = SerieRepository.read(serie2Tags, time, time + 5l);
		LOG.debug("values read [{}]", valuesSerie2);

		LibTimeSeries.close();

	}
}
