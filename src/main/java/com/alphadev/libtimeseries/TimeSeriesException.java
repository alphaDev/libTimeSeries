package com.alphadev.libtimeseries;

public class TimeSeriesException extends RuntimeException {
	private static final long serialVersionUID = -6806193800522318698L;

	public TimeSeriesException(String message) {
		super(message);
	}

	public TimeSeriesException(Throwable cause) {
		super(cause);
	}

	public TimeSeriesException(String message, Throwable cause) {
		super(message, cause);
	}

}
